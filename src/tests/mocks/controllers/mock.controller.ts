import { Delete, Post, Query } from '@nestjs/common';
import { ApiInit, ControllerInit } from 'src/common/decorators';
import { DeviceTokenService, TokenService } from 'src/modules/auth/services';
import { Message } from 'src/modules/base/dtos';
import { DataService } from 'src/modules/data/services';
import { ImageService } from 'src/modules/image/services';
import fsExtra from 'fs-extra';
import { catchError } from 'rxjs/operators';
import { UserService } from 'src/modules/user/services';
import { DeviceService } from 'src/modules/device/services';
import { PersonService } from 'src/modules/person/services';
import { RoomService } from 'src/modules/room/services';
import { ZoneService } from 'src/modules/zone/services';
import { CustomerService } from 'src/modules/customer/services';
import faker from 'faker';
import { NewDeviceDTO } from 'src/modules/device/dtos';
import { Status } from 'src/modules/device/interfaces';

@ControllerInit('mock')
export class MockController {
  constructor(
    private readonly userService: UserService,
    private readonly deviceService: DeviceService,
    private readonly imageService: ImageService,
    private readonly personService: PersonService,
    private readonly roomService: RoomService,
    private readonly zoneService: ZoneService,
    private readonly customerService: CustomerService,
    private readonly dataService: DataService,
    private readonly tokenService: TokenService,
    private readonly deviceTokenService: DeviceTokenService,
  ) {}
  @Delete('clear')
  @ApiInit('DANGEROUS. clear all image and data')
  public async clearData(): Promise<Message> {
    try {
      await this.userService.clearAll();
      await this.deviceService.clearAll();
      await this.imageService.clearAll();
      await this.personService.clearAll();
      await this.roomService.clearAll();
      await this.dataService.clearAll();
      await this.tokenService.clearAll();
      await this.deviceTokenService.clearAll();
      await this.zoneService.clearAll();
      await this.customerService.clearAll();
      fsExtra.emptyDirSync('./uploads');
      return { message: 'Clear successfully!' };
    } catch (error) {
      catchError(error);
    }
  }

  @Post('device')
  @ApiInit('Mock data device')
  public async mockDevice(@Query() count: number): Promise<Message> {
    try {
      const fakerDevice = (): NewDeviceDTO => {
        return {
          deviceName: faker.name.findName(),
          deviceMAC: faker.internet.mac(),
          deviceType: faker.lorem.word(),
          SN: faker.git.shortSha(),
          IPAddress: faker.internet.ip(),
          version: 'v1.1.0',
          port: faker.datatype.number(8080),
          status: Status.OFF,
        };
      };
      for (let i = 0; i < count; i++) {
        await this.deviceService.newDevice(fakerDevice());
      }
      return { message: 'Mock successfully!' };
    } catch (error) {
      catchError(error);
    }
  }
  @Post('room')
  @ApiInit('Mock data room and customer')
  public async mockRoom(): Promise<Message> {
    try {
      const customer = await this.customerService.newCustomer({
        nameOfBookingPerson: faker.datatype.string(),
        numberOfPerson: 10,
        personIds: ['60cca0e5e7933e2380e3b64b', '60cca0e5e7933e2380e3b64b'],
        roomIds: ['60ccc9a79f27cadc96cf2fc8'],
      });
      return { message: 'Mock successfully!!' };
    } catch (error) {
      catchError(error);
    }
  }
}

import { Module } from '@nestjs/common';
import { BaseModule } from 'src/modules/base/base.module';
import { MockController } from './controllers';

@Module({
  imports: [BaseModule],
  controllers: [MockController],
})
export class MockModule {}

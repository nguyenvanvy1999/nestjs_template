import { Module } from '@nestjs/common';
import { DataModule } from 'src/modules/data/data.module';
import { DeviceGateway } from './controllers';

@Module({
  imports: [DataModule],
  providers: [DeviceGateway],
  exports: [DeviceGateway],
})
export class DeviceGatewayModule {}

import {
  MessageBody,
  SubscribeMessage,
  WsException,
  WsResponse,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { AppLogger } from 'src/common/logger';
import { Message } from 'src/modules/base/dtos';
import { DataDTO } from 'src/modules/data/dtos';
import { DataService } from 'src/modules/data/services';
import jwt from 'jsonwebtoken';
import { AppConfigService } from 'src/common/env';
import { WebSocketGatewayInit } from 'src/common/decorators';

const port = parseInt(process.env.DEVICE_PORT) || 4000;

@WebSocketGatewayInit(port)
export class DeviceGateway {
  constructor(
    private readonly dataService: DataService,
    private readonly config: AppConfigService,
  ) {}
  async handleConnection(client: Socket) {
    try {
      const token = client.handshake.headers.token;
      if (!token) throw new WsException('No token found!');
      const decoded = jwt.verify(
        token,
        this.config.get<string>('DEVICE_SECRET'),
      ) as any;
      if (!decoded) throw new WsException('Token wrong!');
      client.handshake.headers.device = decoded.device;
      AppLogger.log(`New client connected! ${port}`);
      client.emit('connection', `Successfully connected to server ${port}`);
    } catch (error) {
      client.disconnect();
      AppLogger.error('Client auth failed!');
    }
  }

  handleDisconnect() {
    AppLogger.log(`Client disconnect! ${port}`);
  }

  @SubscribeMessage('send-data')
  async sendDataTest(@MessageBody() data: any): Promise<WsResponse<Message>> {
    try {
      console.log(data);
      // await this.dataService.newUnknownPerson(data);
      return {
        event: 'server-message',
        data: { message: 'Save data successfully!' },
      };
    } catch (error) {
      throw new WsException(error);
    }
  }
  @SubscribeMessage('save-data')
  async sendData(@MessageBody() data: DataDTO): Promise<WsResponse<Message>> {
    try {
      await this.dataService.newData(data);
      return {
        event: 'save-data',
        data: { message: 'Save data successfully!' },
      };
    } catch (error) {
      throw new WsException(error);
    }
  }

  public async unknownPerson(
    @MessageBody() data: DataDTO,
  ): Promise<WsResponse<Message>> {
    try {
      await this.dataService.newData(data);
      return {
        event: 'server-message',
        data: { message: 'Save successfully!' },
      };
    } catch (error) {
      throw new WsException(error);
    }
  }

  @SubscribeMessage('message')
  async message(): Promise<WsResponse<string>> {
    try {
      return {
        event: 'server-message',
        data: 'Hello world!',
      };
    } catch (error) {
      throw new WsException(error);
    }
  }
}

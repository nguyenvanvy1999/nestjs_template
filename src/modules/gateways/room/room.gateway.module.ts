import { Module } from '@nestjs/common';
import { RoomModule } from 'src/modules/room/room.module';
import { RoomGateway } from './controllers';
@Module({
  imports: [RoomModule],
  providers: [RoomGateway],
  exports: [RoomGateway],
})
export class RoomGatewayModule {}

import {
  SubscribeMessage,
  WebSocketServer,
  WsException,
  WsResponse,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { AppLogger } from 'src/common/logger';
import jwt from 'jsonwebtoken';
import { WebSocketGatewayInit } from 'src/common/decorators';
import { OnEvent } from '@nestjs/event-emitter';
import { AnimalWarningEvent, OverPersonEvent } from 'src/modules/room/events';
import { AppConfigService } from 'src/common/env';
import { RoomService } from 'src/modules/room/services';
import { Room } from 'src/modules/room/models';
const port = parseInt(process.env.ROOM_PORT) || 5000;

@WebSocketGatewayInit(port)
export class RoomGateway {
  @WebSocketServer() private readonly server: Socket;
  constructor(
    private readonly config: AppConfigService,
    private readonly roomService: RoomService,
  ) {}
  public async handleConnection(client: Socket) {
    try {
      const token = client.handshake.headers.token;
      if (!token) throw new WsException('No token found!');
      const decoded = jwt.verify(
        token,
        this.config.get<string>('JWT_SECRET'),
      ) as any;
      if (!decoded) throw new WsException('Token wrong!');
      client.handshake.headers.device = decoded.device;
      AppLogger.log(`New client connected! ${port}`);
      client.emit('connection', `Successfully connected to server ${port}`);
    } catch (error) {
      client.disconnect();
      AppLogger.error('Client auth failed!');
    }
  }
  public handlerDisconnect() {
    AppLogger.log(`Client disconnect! ${port}`);
  }

  @OnEvent('room.over')
  public addPerson(event: OverPersonEvent): boolean {
    try {
      return this.server.emit('room.over', event);
    } catch (error) {
      throw new WsException(error);
    }
  }
  @OnEvent('room.animal')
  public animalWarning(event: AnimalWarningEvent): boolean {
    try {
      return this.server.emit('room.animal', event);
    } catch (error) {
      throw new WsException(error);
    }
  }

  @SubscribeMessage('get_all_room_status')
  public async overPerson(): Promise<WsResponse<Room[]>> {
    try {
      const rooms = await this.roomService.findAllRoom();
      return { event: 'total_room', data: rooms };
    } catch (error) {
      throw new WsException(error);
    }
  }
}

import { Module } from '@nestjs/common';
import { DeviceModule } from '../device/device.module';
import { DeviceGatewayModule } from './device/device.gateway.module';
import { RoomGatewayModule } from './room/room.gateway.module';

@Module({
  imports: [DeviceModule, DeviceGatewayModule, RoomGatewayModule],
})
export class GatewayModule {}

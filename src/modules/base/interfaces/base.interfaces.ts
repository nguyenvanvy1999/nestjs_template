export interface BaseInterface {
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  info?: any;
}

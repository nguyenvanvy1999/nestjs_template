import { ApiProperty } from '@nestjs/swagger';

export class Message {
  @ApiProperty({
    type: String,
    readOnly: true,
    required: true,
    description: 'Message from server',
  })
  message: string;
}

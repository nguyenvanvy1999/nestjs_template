import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsInt, Min } from 'class-validator';

export class Pagination {
  @ApiProperty({
    description: 'Skip',
    required: false,
    default: 0,
    type: Number,
  })
  @IsInt()
  @Min(0)
  @Type(() => Number)
  readonly skip?: number = 0;

  @ApiProperty({
    description: 'Limit',
    required: false,
    default: 20,
    type: Number,
  })
  @IsInt()
  @Min(1)
  @Type(() => Number)
  readonly limit?: number = 20;
}

import { Module } from '@nestjs/common';
import { CustomerModule } from '../customer/customer.module';
import { DataModule } from '../data/data.module';
import { DeviceModule } from '../device/device.module';
import { ImageModule } from '../image/image.module';
import { PersonModule } from '../person/person.module';
import { RoomModule } from '../room/room.module';
import { UserModule } from '../user/user.module';
import { VisitorModule } from '../visitor/visitor.module';
import { ZoneModule } from '../zone/zone.module';

@Module({
  imports: [
    UserModule,
    DeviceModule,
    ImageModule,
    PersonModule,
    RoomModule,
    ZoneModule,
    DataModule,
    CustomerModule,
    VisitorModule,
  ],
  exports: [
    UserModule,
    DeviceModule,
    ImageModule,
    PersonModule,
    RoomModule,
    ZoneModule,
    DataModule,
    CustomerModule,
    VisitorModule,
  ],
})
export class BaseModule {}

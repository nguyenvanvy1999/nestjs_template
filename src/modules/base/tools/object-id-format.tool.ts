import { Types } from 'mongoose';
import { ObjectID } from 'mongodb';
export const ObjectId = (_id?: string): Types.ObjectId => {
  if (_id === undefined) return Types.ObjectId();
  return ObjectID.isValid(_id) ? Types.ObjectId(_id) : null;
};

export const ObjectIds = (_ids: string[]): Types.ObjectId[] => {
  return _ids.map((_id) => ObjectId(_id));
};

import { Base } from '../models';

export const formatOne = (data: Base) => {
  const _ = data.toJSON();
  const { createdAt, updatedAt, deletedAt, info, id, __v, ...result } = _;
  return result;
};

export const formatMany = (datas: Base[]) => {
  const results = [];
  datas.forEach((data) => {
    const tmp = formatOne(data);
    results.push(tmp);
  });
  return { datas: results, count: results.length };
};

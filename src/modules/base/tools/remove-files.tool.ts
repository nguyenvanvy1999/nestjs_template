import fs from 'fs';

export const removeFiles = (files: Express.Multer.File[]) =>
  files.forEach((file) => fs.unlinkSync(file.path));

export const removeFilesByPath = (paths: string[] | string) =>
  Array.isArray(paths)
    ? paths.forEach((path) => fs.unlinkSync(path))
    : fs.unlinkSync(paths);

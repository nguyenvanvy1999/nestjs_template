import { Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { schemaOption } from 'src/common/mongo';

export type ServiceDocument = Service & Document;

@Schema(schemaOption)
export class Service extends Base {}

export const ServiceSchema = SchemaFactory.createForClass(Service);

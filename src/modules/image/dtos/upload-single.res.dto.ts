import { ApiProperty } from '@nestjs/swagger';

export class UploadSingleResDTO {
  @ApiProperty({ description: '_id of image', required: true })
  readonly _id: string;

  @ApiProperty({
    description: 'Originalname of file',
    readOnly: true,
    required: true,
  })
  readonly originalname: string;

  @ApiProperty({
    description: 'Filename when saved in server',
    required: true,
    readOnly: true,
  })
  readonly filename: string;
}

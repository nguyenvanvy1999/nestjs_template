export { NewImageDTO } from './new-image.dto';
export { UploadMultiResDTO } from './upload-multi.res.dto';
export { UploadSingleResDTO } from './upload-single.res.dto';

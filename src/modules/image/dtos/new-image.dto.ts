import { CheckString, MongoId } from 'src/common/decorators';

export class NewImageDTO {
  @CheckString()
  name: string;

  @MongoId(true)
  readonly deviceId: string;

  @MongoId(true)
  readonly personId: string;

  @CheckString()
  path: string;
}

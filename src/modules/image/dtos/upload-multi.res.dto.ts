import { ApiProperty } from '@nestjs/swagger';
import { UploadSingleResDTO } from './upload-single.res.dto';

export class UploadMultiResDTO {
  @ApiProperty({
    description: 'Files info',
    type: [UploadSingleResDTO],
    required: true,
  })
  files: UploadSingleResDTO[];

  @ApiProperty({ description: 'Count of file', type: Number, required: true })
  count: number;
}

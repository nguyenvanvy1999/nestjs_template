import {
  Post,
  UploadedFiles,
  UploadedFile,
  Get,
  Req,
  Res,
  Query,
} from '@nestjs/common';
import {
  UploadSingle,
  UploadMulti,
  ControllerInit,
  DeviceAuth,
  ApiInit,
} from 'src/common/decorators';
import { Id } from 'src/modules/auth/decorators';
import { UploadMultiResDTO, UploadSingleResDTO } from '../dtos';
import { ImageService } from '../services';
import fs from 'fs';
import path from 'path';
import { Request, Response } from 'express';
import { Public } from 'src/modules/auth/guards';
import { catchError } from 'src/common/exceptions';
import { Image } from '../models';

@ControllerInit('image')
@DeviceAuth()
export class ImageController {
  constructor(private readonly imageService: ImageService) {}
  @Post('upload-single')
  @ApiInit('Upload single file', UploadSingleResDTO)
  @UploadSingle('image')
  public async uploadSingle(
    @UploadedFile() file: Express.Multer.File,
    @Id() id: string,
  ): Promise<UploadSingleResDTO> {
    try {
      const image = await this.imageService.newImage({
        name: file.filename,
        deviceId: id,
        personId: null,
        path: file.path,
      });
      return {
        _id: image._id.toHexString(),
        originalname: file.originalname,
        filename: file.filename,
      };
    } catch (error) {
      catchError(error);
    }
  }

  @Post('upload-multi-local')
  @ApiInit('Upload multi file', UploadMultiResDTO)
  @UploadMulti('images', 20)
  public async uploadMulti(
    @UploadedFiles() files: Express.Multer.File[],
    @Id() id: string,
  ): Promise<UploadMultiResDTO> {
    try {
      const response = [];
      await Promise.all(
        files.map(async (file) => {
          const image = await this.imageService.newImage({
            name: file.filename,
            deviceId: id,
            personId: null,
            path: file.path,
          });
          const _ = {
            _id: image._id.toHexString(),
            originalname: file.originalname,
            filename: file.filename,
          };
          response.push(_);
        }),
      );
      return { files: response, count: response.length };
    } catch (error) {
      catchError(error);
    }
  }

  @Get('sync-data')
  @ApiInit('Download image', String)
  @Public()
  public async syncData(
    @Query('image') image: string,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      //FIXME:
      const test = path.join(__dirname, '../../../../uploads/1.png');
      const dst = fs.createWriteStream(test);
      req.pipe(dst);
      dst.on('drain', () => req.resume());
      req.on('end', () => res.sendStatus(200));
    } catch (error) {
      catchError(error);
    }
  }

  @Get()
  @ApiInit('Get all image', [Image])
  public async getAllImage(): Promise<Image[]> {
    try {
      return await this.imageService.findAll();
    } catch (error) {
      catchError(error);
    }
  }

  @Get('download')
  @ApiInit('Download image', String)
  @Public()
  public async downloadImage(
    @Res() res: Response,
    @Query('id') id: string,
  ): Promise<any> {
    try {
      const image = await this.imageService.findById(id);
      return res.download(path.join(__dirname, '../../../../', image.path));
    } catch (error) {
      catchError(error);
    }
  }

  @Get('load-image')
  @ApiInit('Load image', String)
  public async loadImage(
    @Res() res: Response,
    @Query('id') id: string,
  ): Promise<any> {
    try {
      const image = await this.imageService.findById(id);
      return res.sendFile(path.join(__dirname, '../../../../', image.path));
    } catch (error) {
      catchError(error);
    }
  }
}

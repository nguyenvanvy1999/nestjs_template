import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { DeviceJwtStrategy } from '../auth/strategies';
import { jwtDeviceConfig } from '../auth/auth.config';
import { DeviceModule } from '../device/device.module';
import { ImageController } from './controllers';
import { ImageSchema, Image } from './models';
import { ImageService } from './services';

@Module({
  imports: [
    PassportModule,
    DeviceModule,
    JwtModule.registerAsync(jwtDeviceConfig),
    MongooseModule.forFeature([{ name: Image.name, schema: ImageSchema }]),
  ],
  controllers: [ImageController],
  providers: [ImageService, DeviceJwtStrategy],
  exports: [ImageService],
})
export class ImageModule {}

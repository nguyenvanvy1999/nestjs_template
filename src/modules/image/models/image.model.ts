import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types, Schema as MongooseSchema } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { schemaOption } from 'src/common/mongo';

export type ImageDocument = Image & Document;
@Schema(schemaOption)
export class Image extends Base {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: String, required: true })
  path: string;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    required: true,
    immutable: true,
    ref: 'Device',
  })
  deviceId: Types.ObjectId;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    required: false,
    immutable: true,
    ref: 'Person',
  })
  personId: Types.ObjectId;
}

export const ImageSchema = SchemaFactory.createForClass(Image);

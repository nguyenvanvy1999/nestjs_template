import { Test, TestingModule } from '@nestjs/testing';
import { ImageController } from '../controllers';
import { ImageService } from '../services';

describe('ImageController', () => {
  let imageController: ImageController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ImageController],
      providers: [ImageService],
    }).compile();
    imageController = app.get<ImageController>(ImageController);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(imageController).toBeDefined();
    });
  });
});

import { Test, TestingModule } from '@nestjs/testing';
import { ImageService } from '../services';

describe('ImageService', () => {
  let imageService: ImageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ImageService],
    }).compile();
    imageService = module.get<ImageService>(ImageService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(imageService).toBeDefined();
    });
  });
});

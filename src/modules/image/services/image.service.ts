import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { catchError } from 'src/common/exceptions';
import { Pagination } from 'src/modules/base/dtos';
import { removeFilesByPath } from 'src/modules/base/tools';
import { NewImageDTO } from '../dtos';
import { Image, ImageDocument } from '../models';

@Injectable()
export class ImageService {
  constructor(
    @InjectModel(Image.name) private readonly imageModel: Model<ImageDocument>,
  ) {}
  public async newImage(data: NewImageDTO): Promise<Image> {
    try {
      const newImage = new this.imageModel({
        _id: Types.ObjectId(),
        ...data,
        personId: data.personId === 'Unknown' ? null : data.personId,
        deviceId: data.deviceId,
      });
      await newImage.save();
      return newImage;
    } catch (error) {
      catchError(error);
    }
  }
  public async findById(id: string): Promise<Image> {
    try {
      return await this.imageModel.findById(id);
    } catch (error) {
      catchError(error);
    }
  }
  public async findByDevice(
    deviceId: string,
    pagination?: Pagination,
  ): Promise<Image[]> {
    try {
      return await this.imageModel
        .find({ deviceId })
        .skip(pagination?.skip)
        .limit(pagination?.limit)
        .lean();
    } catch (error) {
      catchError(error);
    }
  }

  public async removeImagesByIds(imageIds: string[]): Promise<any> {
    try {
      const images = await this.imageModel
        .find({ _id: { $in: imageIds } })
        .lean();
      images.forEach((image) =>
        removeFilesByPath(`${process.env.ROOT_PATH}${image.path}`),
      );
      await this.imageModel.deleteMany({ _id: { $in: imageIds } });
      return;
    } catch (error) {
      catchError(error);
    }
  }

  public async updatePerson(imageId: string, personId: string): Promise<Image> {
    try {
      return await this.imageModel.findOneAndUpdate(
        { _id: imageId },
        { personId: personId },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }
  public async findAll(pagination?: Pagination): Promise<Image[]> {
    try {
      return await this.imageModel
        .find({})
        .skip(pagination?.skip)
        .limit(pagination?.limit)
        .lean();
    } catch (error) {
      catchError(error);
    }
  }
  public async clearAll(): Promise<any> {
    try {
      return await this.imageModel.deleteMany();
    } catch (error) {
      catchError(error);
    }
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { AppService } from '../services/app.service';

describe('AppService', () => {
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [AppService],
    }).compile();

    appService = app.get<AppService>(AppService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(appService).toBeDefined();
    });
  });
});

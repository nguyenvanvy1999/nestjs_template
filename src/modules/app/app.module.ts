import { Module } from '@nestjs/common';
import { CommonModule } from 'src/common/common.module';
import { MockModule } from 'src/tests/mocks/mock.module';
import { BaseModule } from '../base/base.module';
import { AppController } from './controllers';
import { AppService } from './services';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { GatewayModule } from '../gateways/gateway.module';

@Module({
  imports: [
    EventEmitterModule.forRoot({
      wildcard: false,
      delimiter: '.',
      newListener: true,
      removeListener: true,
      maxListeners: 10,
      verboseMemoryLeak: true,
      ignoreErrors: false,
    }),
    CommonModule,
    MockModule,
    BaseModule,
    GatewayModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

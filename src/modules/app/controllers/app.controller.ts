import { Controller, Get } from '@nestjs/common';
import { ApiInit } from 'src/common/decorators';
import { AppService } from '../services/app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @ApiInit('Hello world!')
  getHello(): string {
    return this.appService.getHello();
  }
}

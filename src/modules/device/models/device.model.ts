import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { schemaOption } from 'src/common/mongo';
import { Status } from '../interfaces';

export type DeviceDocument = Device & Document;

@Schema(schemaOption)
export class Device extends Base {
  @Prop({ required: true, type: String })
  deviceName: string;

  @Prop({ required: true, type: String, immutable: true })
  deviceType: string;

  @Prop({ required: true, type: String, immutable: true })
  SN: string;

  @Prop({ type: String, required: true, trim: true })
  IPAddress: string;

  @Prop({ type: String, required: true, trim: true, immutable: true })
  deviceMAC: string;

  @Prop({ type: String, required: true, trim: true })
  version: string;

  @Prop({ type: Number, required: true })
  port: number;

  @Prop({ type: Status, enum: Object.values(Status), default: Status.OFF })
  status: Status;
}

export const DeviceSchema = SchemaFactory.createForClass(Device);

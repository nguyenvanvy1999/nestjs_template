import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsInt, IsIP, IsMACAddress, IsNotEmpty } from 'class-validator';
import { CheckString } from 'src/common/decorators';
import { Status } from '../interfaces';

export class NewDeviceDTO {
  @ApiProperty({ description: 'Device name', type: String, required: true })
  @CheckString()
  readonly deviceName: string;

  @ApiProperty({ description: 'Device type', type: String, required: true })
  @CheckString()
  readonly deviceType: string;

  @ApiProperty({ description: 'Serial number', required: true, type: String })
  @CheckString()
  readonly SN: string;

  @ApiProperty({ description: 'IP address', type: String, required: true })
  @IsIP()
  @IsNotEmpty()
  readonly IPAddress: string;

  @ApiProperty({ description: 'Device MAC', type: String, required: true })
  @IsMACAddress()
  @IsNotEmpty()
  readonly deviceMAC: string;

  @ApiProperty({ description: 'Device version', type: String, required: true })
  @CheckString()
  readonly version: string;

  @ApiProperty({ description: 'Port number', type: Number, required: true })
  @IsInt()
  @IsNotEmpty()
  readonly port: number;

  @ApiProperty({
    description: 'Device status',
    type: String,
    enum: Status,
    default: Status.OFF,
  })
  @IsEnum(Status)
  readonly status: Status;
}

import { OmitType } from '@nestjs/swagger';
import { NewDeviceDTO } from './new-device.dto';

export class UpdateDeviceDTO extends OmitType(NewDeviceDTO, [
  'SN',
  'deviceMAC',
  'deviceType',
]) {}

import { Test, TestingModule } from '@nestjs/testing';
import { DeviceTokenService } from 'src/modules/auth';
import { DeviceController } from '../controllers';
import { DeviceService } from '../services';

describe('DeviceController', () => {
  let deviceController: DeviceService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [DeviceController],
      providers: [DeviceService, DeviceTokenService],
    }).compile();
    deviceController = app.get<DeviceService>(DeviceService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(deviceController).toBeDefined();
    });
  });
});

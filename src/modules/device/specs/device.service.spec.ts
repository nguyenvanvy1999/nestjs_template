import { Test, TestingModule } from '@nestjs/testing';
import { DeviceService } from '../services';

describe('DeviceService', () => {
  let deviceService: DeviceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DeviceService],
    }).compile();
    deviceService = module.get<DeviceService>(DeviceService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(deviceService).toBeDefined();
    });
  });
});

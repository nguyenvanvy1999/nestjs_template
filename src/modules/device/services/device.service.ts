import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { catchError } from 'src/common/exceptions';
import { Pagination } from 'src/modules/base/dtos';
import { NewDeviceDTO, UpdateDeviceDTO } from '../dtos';
import { Device, DeviceDocument } from '../models';

@Injectable()
export class DeviceService {
  constructor(
    @InjectModel(Device.name)
    private readonly deviceModel: Model<DeviceDocument>,
  ) {}

  public async newDevice(device: NewDeviceDTO): Promise<Device> {
    try {
      const newDevice = new this.deviceModel({
        _id: Types.ObjectId(),
        ...device,
      });
      await newDevice.save();
      return newDevice;
    } catch (error) {
      catchError(error);
    }
  }
  public async findBySN(SN: string): Promise<Device> {
    try {
      return await this.deviceModel.findOne({ SN });
    } catch (error) {
      catchError(error);
    }
  }
  public async findById(_id: string): Promise<Device> {
    try {
      return await this.deviceModel.findById(_id);
    } catch (error) {
      catchError(error);
    }
  }

  public async editDevice(
    deviceId: string,
    data: UpdateDeviceDTO,
  ): Promise<Device> {
    try {
      return await this.deviceModel.findOneAndUpdate(
        { _id: deviceId },
        { $set: { ...data } },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }
  public async findAll(pagination?: Pagination): Promise<Device[]> {
    try {
      return await this.deviceModel
        .find()
        .skip(pagination?.skip)
        .limit(pagination?.limit)
        .lean();
    } catch (error) {
      catchError(error);
    }
  }
  public async clearAll(): Promise<any> {
    try {
      return await this.deviceModel.deleteMany();
    } catch (error) {
      catchError(error);
    }
  }
}

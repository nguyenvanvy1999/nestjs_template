import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { DeviceJwtStrategy } from '../auth/strategies';
import { DeviceToken, DeviceTokenSchema } from '../auth/models';
import { DeviceTokenService } from '../auth/services';
import { jwtDeviceConfig } from '../auth/auth.config';
import { DeviceController } from './controllers';
import { Device, DeviceSchema } from './models';
import { DeviceService } from './services';

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync(jwtDeviceConfig),
    MongooseModule.forFeature([
      { name: Device.name, schema: DeviceSchema },
      { name: DeviceToken.name, schema: DeviceTokenSchema },
    ]),
  ],
  controllers: [DeviceController],
  providers: [DeviceService, DeviceTokenService, DeviceJwtStrategy],
  exports: [DeviceService, DeviceTokenService],
})
export class DeviceModule {}

import {
  Get,
  Post,
  Req,
  UnauthorizedException,
  UseGuards,
  Query,
  BadRequestException,
  Body,
  Patch,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiQuery,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Id } from 'src/modules/auth/decorators';
import { Public, DeviceJwtAuthGuard } from 'src/modules/auth/guards';
import { DeviceTokenService } from 'src/modules/auth/services';
import { TokenRes } from 'src/modules/auth/dtos';
import { DeviceService } from '../services';
import { Request } from 'express';
import { Device } from '../models';
import { ControllerInit, ApiInit } from 'src/common/decorators';
import { catchError, ErrorRes } from 'src/common/exceptions';
import { NewDeviceDTO, UpdateDeviceDTO } from '../dtos';
import { isUUID } from 'class-validator';

@ControllerInit('device')
@ApiUnauthorizedResponse({ type: ErrorRes })
@UseGuards(DeviceJwtAuthGuard)
export class DeviceController {
  constructor(
    private readonly deviceService: DeviceService,
    private readonly deviceTokenService: DeviceTokenService,
  ) {}

  @Post('active')
  @ApiInit('Active device', TokenRes)
  @Public()
  @ApiQuery({ name: 'SN', type: String })
  public async activeDevice(
    @Query('SN') SN: string,
    @Req() req: Request,
  ): Promise<TokenRes> {
    try {
      if (!SN) throw new BadRequestException('No SN found!');
      const device = await this.deviceService.findBySN(SN);
      if (!device) throw new UnauthorizedException('SN Device wrong!');
      const _id = device._id.toHexString();
      const accessToken = await this.deviceTokenService.createAccessToken(_id);
      const refreshToken = await this.deviceTokenService.createRefreshToken(
        req,
        _id,
      );
      return {
        accessToken,
        refreshToken,
      };
    } catch (error) {
      catchError(error);
    }
  }

  @Get('')
  @ApiBearerAuth()
  @ApiInit('Get device info', Device)
  @ApiBadRequestResponse({ description: 'No device!', type: ErrorRes })
  public async deviceInfo(@Id() id: string): Promise<Device> {
    try {
      const device = await this.deviceService.findById(id);
      if (!device) throw new BadRequestException('No device');
      return device;
    } catch (error) {
      catchError(error);
    }
  }

  @Get(':id')
  @ApiInit('Find device by id', Device)
  @Public()
  public async findDeviceById(@Query('id') id: string): Promise<Device> {
    try {
      return await this.deviceService.findById(id);
    } catch (error) {
      catchError(error);
    }
  }

  @Get('refresh')
  @ApiInit('Refresh token', TokenRes)
  @ApiUnauthorizedResponse({
    description: 'No device token found!',
    type: ErrorRes,
  })
  @ApiQuery({ name: 'token', type: String })
  @Public()
  public async refreshToken(@Query('token') token: string): Promise<TokenRes> {
    try {
      if (!token) throw new BadRequestException('No refresh token found!');
      if (!isUUID(token, 4))
        throw new BadRequestException('Refresh token wrong!');
      const device = await this.deviceTokenService.findRefreshTokenByToken(
        token,
      );
      const accessToken = await this.deviceTokenService.createAccessToken(
        device,
      );
      return {
        accessToken,
        refreshToken: token,
      };
    } catch (error) {
      catchError(error);
    }
  }

  @Post()
  @ApiInit('Create new device', Device)
  @Public()
  @ApiBadRequestResponse({
    description: 'Device has been exits!',
    type: ErrorRes,
  })
  public async newDevice(@Body() data: NewDeviceDTO): Promise<Device> {
    try {
      const idDevice = await this.deviceService.findBySN(data.SN);
      if (idDevice) throw new BadRequestException('Device has been exits!');
      return await this.deviceService.newDevice(data);
    } catch (error) {
      catchError(error);
    }
  }

  @Patch()
  @ApiInit('Edit device', Device)
  @ApiBadRequestResponse({ description: 'No device found!', type: ErrorRes })
  public async EditDevice(
    @Id() id: string,
    @Body() data: UpdateDeviceDTO,
  ): Promise<Device> {
    try {
      const device = await this.deviceService.findById(id);
      if (!device) throw new BadRequestException('No device found!');
      return await this.deviceService.editDevice(id, data);
    } catch (error) {
      catchError(error);
    }
  }
}

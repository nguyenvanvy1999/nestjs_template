import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { catchError } from 'src/common/exceptions';
import { Pagination } from 'src/modules/base/dtos';
import { ChangerCountDTO, CheckInRoomDTO } from '../dtos';
import { OverPersonEvent } from '../events';
import { Room, RoomDocument } from '../models';

@Injectable()
export class RoomService {
  constructor(
    @InjectModel(Room.name) private readonly roomModel: Model<RoomDocument>,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  public async findById(roomId: string): Promise<Room> {
    try {
      return await this.roomModel.findById(roomId);
    } catch (error) {
      catchError(error);
    }
  }

  public async addPersonCount(data: ChangerCountDTO): Promise<Room> {
    try {
      const room = await this.roomModel.findOneAndUpdate(
        { _id: data.roomId },
        { $inc: { countPerson: data.count } },
        { new: true },
      );
      if (room.specifiedPerson < room.countPerson) {
        const overPersonEvent: OverPersonEvent = {
          roomId: room._id,
          specifiedPerson: room.specifiedPerson,
          personInRoomNow: room.countPerson,
          time: new Date().toLocaleString(),
        };
        this.eventEmitter.emit('room.over', overPersonEvent);
      }
      return room;
    } catch (error) {
      catchError(error);
    }
  }

  public async rmvPersonCount(data: ChangerCountDTO): Promise<Room> {
    try {
      const room = await this.roomModel.findOneAndUpdate(
        { _id: data.roomId },
        { $inc: { countPerson: -data.count } },
        { new: true },
      );
      if (room.specifiedPerson < room.countPerson) {
        const overPersonEvent: OverPersonEvent = {
          roomId: room._id,
          specifiedPerson: room.specifiedPerson,
          personInRoomNow: room.countPerson,
          time: new Date().toLocaleString(),
        };
        this.eventEmitter.emit('room.over', overPersonEvent);
      }
      return room;
    } catch (error) {
      catchError(error);
    }
  }

  public async checkInRoom(data: CheckInRoomDTO): Promise<Room> {
    try {
      return await this.roomModel.findOneAndUpdate(
        { _id: data.roomId },
        { ...data },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }

  public async checkOutRoom(roomId: string): Promise<Room> {
    try {
      return await this.roomModel.findOneAndUpdate(
        { _id: roomId },
        { customerId: null, countPerson: 0 },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }

  public async checkStatusRoom(roomId: string): Promise<boolean> {
    try {
      const room = await this.roomModel.findById(roomId);
      return room.customerId === null;
    } catch (error) {
      catchError(error);
    }
  }

  public async findAllRoom(pagination?: Pagination): Promise<Room[]> {
    try {
      return await this.roomModel
        .find()
        .limit(pagination?.limit)
        .skip(pagination?.skip);
    } catch (error) {
      catchError(error);
    }
  }

  public async findAllRoomNull(pagination?: Pagination): Promise<Room[]> {
    try {
      return await this.roomModel
        .find({ customerId: null })
        .limit(pagination?.limit)
        .skip(pagination?.skip);
    } catch (error) {
      catchError(error);
    }
  }
  public async clearAll(): Promise<any> {
    try {
      return await this.roomModel.deleteMany();
    } catch (error) {
      catchError(error);
    }
  }
  public async createRoom(name: string): Promise<Room> {
    try {
      const newRoom = new this.roomModel({ _id: Types.ObjectId(), name });
      await newRoom.save();
      return newRoom;
    } catch (error) {
      catchError(error);
    }
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { RoomService } from '../services';

describe('RoomService', () => {
  let roomService: RoomService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RoomService],
    }).compile();
    roomService = module.get<RoomService>(RoomService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(roomService).toBeDefined();
    });
  });
});

import { Test, TestingModule } from '@nestjs/testing';
import { RoomController } from '../controllers';
import { RoomService } from '../services';
describe('RoomController', () => {
  let roomController: RoomController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [RoomController],
      providers: [RoomService],
    }).compile();
    roomController = app.get<RoomController>(RoomController);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(roomController).toBeDefined();
    });
  });
});

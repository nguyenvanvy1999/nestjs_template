export class OverPersonEvent {
  roomId: string;
  personInRoomNow: number;
  specifiedPerson: number;
  time: string;
}

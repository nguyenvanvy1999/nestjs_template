import {
  BadRequestException,
  Body,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { ApiBadGatewayResponse, ApiBadRequestResponse } from '@nestjs/swagger';
import { isMongoId } from 'class-validator';
import { ApiInit, ControllerInit } from 'src/common/decorators';
import { catchError, ErrorRes } from 'src/common/exceptions';
import { Message, Pagination } from 'src/modules/base/dtos';
import { ChangerCountDTO, CheckInRoomDTO } from '../dtos';
import { AnimalWarningEvent } from '../events';
import { Room } from '../models';
import { RoomService } from '../services';

@ControllerInit('room')
export class RoomController {
  constructor(
    private readonly roomService: RoomService,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  @Post('check-in')
  @ApiInit('Check in room', Room)
  @ApiBadRequestResponse({
    description: 'Room has been have customer!',
    type: ErrorRes,
  })
  public async checkIn(@Body() data: CheckInRoomDTO): Promise<Room> {
    try {
      const isNull = await this.roomService.checkStatusRoom(data.roomId);
      if (isNull) throw new BadRequestException('Room has been have customer');
      return await this.roomService.checkInRoom(data);
    } catch (error) {
      catchError(error);
    }
  }

  @Post('check-out')
  @ApiInit('Check out room', Room)
  @ApiBadGatewayResponse({ description: 'RoomId wrong!', type: ErrorRes })
  public async checkOutRoom(@Query('id') roomId: string): Promise<Room> {
    try {
      if (!isMongoId(roomId)) throw new BadRequestException('RoomId wrong!');
      return await this.roomService.checkOutRoom(roomId);
    } catch (error) {
      catchError(error);
    }
  }

  @Get('empty-room')
  @ApiInit('Get all empty room', [Room])
  public async getAllEmptyRoom(
    @Query() pagination?: Pagination,
  ): Promise<Room[]> {
    try {
      return await this.roomService.findAllRoomNull(pagination);
    } catch (error) {
      catchError(error);
    }
  }
  @Get()
  @ApiInit('Get all room', [Room])
  public async getAllRoom(@Query() pagination?: Pagination): Promise<Room[]> {
    try {
      return await this.roomService.findAllRoom(pagination);
    } catch (error) {
      catchError(error);
    }
  }
  @Patch('add-person')
  @ApiInit('Add person count in room', Room)
  public async changeCount(@Body() data: ChangerCountDTO): Promise<Room> {
    try {
      return await this.roomService.addPersonCount(data);
    } catch (error) {
      catchError(error);
    }
  }
  @Patch('rm-person')
  @ApiInit('Remove person count in room', Room)
  public async rmCount(@Body() data: ChangerCountDTO): Promise<Room> {
    try {
      return await this.roomService.rmvPersonCount(data);
    } catch (error) {
      catchError(error);
    }
  }
  @Post('animal/:id')
  @ApiInit('Animal in room', Message)
  public async warningAnimal(@Param('id') roomId: string): Promise<Message> {
    try {
      const animal: AnimalWarningEvent = {
        roomId,
        time: new Date().toLocaleString(),
      };
      this.eventEmitter.emit('room.animal', animal);
      return { message: 'OK!' };
    } catch (error) {
      catchError(error);
    }
  }
}

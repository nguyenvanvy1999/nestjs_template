import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types, Schema as MongooseSchema } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { schemaOption } from 'src/common/mongo';

export type RoomDocument = Room & Document;
@Schema(schemaOption)
export class Room extends Base {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: 'Customer',
    required: false,
  })
  customerId: Types.ObjectId;

  @Prop({ type: Number, required: false, default: 0 })
  countPerson: number;

  @Prop({ type: Number, default: 0 })
  specifiedPerson: number;
}

export const RoomSchema = SchemaFactory.createForClass(Room);

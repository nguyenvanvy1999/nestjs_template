import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsInt, IsNotEmpty } from 'class-validator';
import { MongoId } from 'src/common/decorators';

export class ChangerCountDTO {
  @ApiProperty({ description: 'RoomId', type: String, required: true })
  @MongoId(true)
  readonly roomId: string;

  @ApiProperty({
    description: 'Count',
    type: Number,
    required: true,
    default: 1,
  })
  @IsInt()
  @IsNotEmpty()
  @Type(() => Number)
  readonly count: number;
}

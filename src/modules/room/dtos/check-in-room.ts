import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsInt, IsNotEmpty } from 'class-validator';
import { MongoId } from 'src/common/decorators';

export class CheckInRoomDTO {
  @ApiProperty({ description: 'RoomId to edit', required: true, type: String })
  @MongoId(true)
  readonly roomId: string;

  @ApiProperty({
    description: 'CustomerId to edit',
    required: true,
    type: String,
  })
  @MongoId(true)
  readonly customerId: string;

  @ApiProperty({
    description: 'Number of person specified',
    required: true,
    type: Number,
  })
  @IsInt()
  @IsNotEmpty()
  @Type(() => Number)
  readonly specifiedPerson: number;
}

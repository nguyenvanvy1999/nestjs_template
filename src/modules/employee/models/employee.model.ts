import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { schemaOption } from 'src/common/mongo';

export type EmployeeDocument = Employee & Document;

@Schema(schemaOption)
export class Employee extends Base {}

export const EmployeeSchema = SchemaFactory.createForClass(Employee);

import { ControllerInit } from 'src/common/decorators';
import { EmployeeService } from '../services';

@ControllerInit('employee')
export class EmployeeController {
  constructor(private readonly employeeService: EmployeeService) {}
}

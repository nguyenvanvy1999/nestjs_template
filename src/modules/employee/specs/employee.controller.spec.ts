import { Test, TestingModule } from '@nestjs/testing';
import { EmployeeController } from '../controllers';
import { EmployeeService } from '../services';

describe('EmployeeController', () => {
  let employeeController: EmployeeController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [EmployeeController],
      providers: [EmployeeService],
    }).compile();
    employeeController = app.get<EmployeeController>(EmployeeController);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(employeeController).toBeDefined();
    });
  });
});

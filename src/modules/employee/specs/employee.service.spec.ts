import { Test, TestingModule } from '@nestjs/testing';
import { EmployeeService } from '../services';

describe('EmployeeService', () => {
  let employeeService: EmployeeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EmployeeService],
    }).compile();
    employeeService = module.get<EmployeeService>(EmployeeService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(employeeService).toBeDefined();
    });
  });
});

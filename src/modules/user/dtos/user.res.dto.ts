import { ApiProperty, PickType } from '@nestjs/swagger';
import { Types } from 'mongoose';
import { UserDTO } from './user.dto';

export class UserResDTO extends PickType(UserDTO, [
  'firstName',
  'lastName',
  'gender',
]) {
  @ApiProperty({ description: '_id of user', type: String })
  readonly _id: Types.ObjectId;
}

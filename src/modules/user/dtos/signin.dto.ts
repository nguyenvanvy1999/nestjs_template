import { PickType } from '@nestjs/swagger';
import { UserDTO } from './user.dto';

export class SignInDTO extends PickType(UserDTO, ['email', 'password']) {}

import { PartialType } from '@nestjs/swagger';
import { UserDTO } from './user.dto';

export class SignUpDTO extends PartialType(UserDTO) {}

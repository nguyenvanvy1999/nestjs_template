import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsEnum, IsNotEmpty, IsPhoneNumber } from 'class-validator';
import { CheckString, IsPassword } from 'src/common/decorators';
import { Gender } from '../interfaces';

export class UserDTO {
  @ApiProperty({ type: String, required: true, description: 'Email' })
  @IsEmail()
  readonly email: string;

  @ApiProperty({
    type: String,
    name: 'password',
    minLength: 4,
    maxLength: 20,
    description: 'Password',
    required: true,
  })
  @IsPassword()
  password: string;

  @ApiProperty({ type: String, description: 'First Name', required: true })
  @CheckString(true, 5)
  readonly firstName: string;

  @ApiProperty({ type: String, description: 'Last Name', required: true })
  @CheckString(true, 5)
  readonly lastName: string;

  @ApiProperty({
    type: String,
    enum: Gender,
    default: Gender.Undisclosed,
    required: true,
    description: 'Gender',
  })
  @IsNotEmpty()
  @IsEnum(Gender)
  readonly gender: Gender;

  @ApiProperty({ required: true, description: 'Phone number', type: String })
  @IsPhoneNumber('VN')
  @IsNotEmpty()
  readonly phoneNumber: string;
}

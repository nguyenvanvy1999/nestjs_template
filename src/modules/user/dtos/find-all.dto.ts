import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';

export class FindAll {
  @ApiProperty({ description: 'Query', required: false })
  query?: any;

  @ApiProperty({
    description: 'Limit docs',
    required: false,
    type: Number,
    default: undefined,
  })
  @IsInt()
  limit?: number;

  @ApiProperty({
    description: 'Skip docs',
    type: Number,
    required: false,
    default: 0,
  })
  @IsInt()
  skip?: number;

  @ApiProperty({
    description: 'Sort by',
    required: false,
  })
  sort?: any;
}

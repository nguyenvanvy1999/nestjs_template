import { PickType } from '@nestjs/swagger';
import { UserDTO } from './user.dto';
export class UserEditDTO extends PickType(UserDTO, [
  'email',
  'firstName',
  'lastName',
  'gender',
  'phoneNumber',
]) {}

import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsUUID } from 'class-validator';

export class LogOut {
  @ApiProperty({
    description: 'Logout from all device ?',
    type: Boolean,
    default: false,
  })
  @IsBoolean()
  @IsNotEmpty()
  readonly fromAll: boolean;

  @ApiProperty({ description: 'refresh token to Logout', required: false })
  @IsUUID(4)
  readonly refreshToken?: string;
}

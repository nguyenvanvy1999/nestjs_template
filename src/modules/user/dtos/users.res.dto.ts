import { ApiProperty } from '@nestjs/swagger';
import { UserResDTO } from './user.res.dto';

export class UsersResDTO {
  @ApiProperty({ type: [UserResDTO], description: 'users' })
  readonly users: UserResDTO[];

  @ApiProperty({ type: Number, description: 'Count of docs' })
  readonly count: number;
}

import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsPassword } from 'src/common/decorators';
import { UserDTO } from './user.dto';
export class ChangerPasswordDTO extends PickType(UserDTO, ['password']) {
  @ApiProperty({
    description: 'New password',
    type: String,
    minLength: 5,
    maxLength: 20,
    required: true,
  })
  @IsPassword()
  newPassword: string;

  @ApiProperty({
    description: 'Repeat password',
    type: String,
    minLength: 5,
    maxLength: 20,
    required: true,
  })
  @IsPassword()
  readonly repeatPassword: string;
}

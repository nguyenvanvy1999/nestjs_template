import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from '../services';

describe('UserService', () => {
  let userService: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserService],
    }).compile();
    userService = module.get<UserService>(UserService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(userService).toBeDefined();
    });
  });
});

import { Test, TestingModule } from '@nestjs/testing';
import { TokenService } from 'src/modules/auth';
import { UserController } from '../controllers';
import { UserService } from '../services';
import { UserTool } from '../tools';

describe('UserController', () => {
  let userController: UserController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService, UserTool, TokenService],
    }).compile();
    userController = app.get<UserController>(UserController);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(userController).toBeDefined();
    });
  });
});

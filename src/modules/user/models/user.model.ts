import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Model } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { Gender } from '../interfaces';
import { schemaOption } from 'src/common/mongo';

export type UserDocument = User & Document;
export interface IUserDocument extends Model<UserDocument> {
  hashPassword: (password: string) => Promise<string>;
}
@Schema(schemaOption)
export class User extends Base {
  @Prop({ required: true, unique: true, lowercase: true, trim: true })
  email: string;

  @Prop({ required: true, min: 8 })
  password: string;

  @Prop({ trim: true })
  firstName: string;

  @Prop({ trim: true })
  lastName: string;

  @Prop({
    type: String,
    enum: Object.values(Gender),
    default: Gender.Undisclosed,
  })
  gender: Gender;

  @Prop({ type: String, required: true })
  phoneNumber: string;

  @Prop({ required: true, default: false })
  isActive: boolean;

  comparePassword: (password: string) => boolean;
  getFullName: () => string;
}

export const UserSchema = SchemaFactory.createForClass(User);

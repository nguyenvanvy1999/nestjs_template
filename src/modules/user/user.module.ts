import { Logger, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppConfigModule } from 'src/common/env';
import { UserController } from './controllers';
import { UserService } from './services';
import { UserTool } from './tools';
import { userMongoConfig } from './user.config';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { Token, TokenSchema } from '../auth/models';
import { TokenService } from '../auth/services';
import { JwtStrategy } from '../auth/strategies';
import { jwtConfig } from '../auth/auth.config';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Token.name, schema: TokenSchema }]),
    PassportModule,
    JwtModule.registerAsync(jwtConfig),
    AppConfigModule,
    MongooseModule.forFeatureAsync([userMongoConfig]),
  ],
  controllers: [UserController],
  providers: [UserService, UserTool, Logger, JwtStrategy, TokenService],
  exports: [UserService, TokenService],
})
export class UserModule {}

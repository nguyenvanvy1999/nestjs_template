/* eslint-disable @typescript-eslint/no-unused-vars */
import { UserResDTO, UsersResDTO } from '../dtos';
import { User } from '../models';

export class UserTool {
  removeForOne(user: User): UserResDTO {
    try {
      const _ = user.toJSON();
      const {
        email,
        phoneNumber,
        isActive,
        password,
        createdAt,
        updatedAt,
        deletedAt,
        info,
        ...result
      } = _;
      return result;
    } catch (error) {
      throw error;
    }
  }
  removeForMany(users: User[]): UsersResDTO {
    try {
      const results = [];
      users.forEach((user) => {
        const tmp = this.removeForOne(user);
        results.push(tmp);
      });
      return { users: results, count: results.length };
    } catch (error) {
      throw error;
    }
  }
}

import {
  Body,
  Get,
  Patch,
  Post,
  Query,
  Req,
  BadRequestException,
  UseGuards,
  UnauthorizedException,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiOperation,
  ApiUnauthorizedResponse,
  ApiBadRequestResponse,
  ApiQuery,
} from '@nestjs/swagger';
import { UserService } from '../services';
import {
  SignInDTO,
  SignUpDTO,
  UserResDTO,
  UsersResDTO,
  FindAll,
  ChangerPasswordDTO,
  UserEditDTO,
  LogOut,
} from '../dtos';
import { UserTool } from '../tools';
import { Request } from 'express';
import { TokenService } from 'src/modules/auth/services';
import { Id } from 'src/modules/auth/decorators';
import { TokenRes } from 'src/modules/auth/dtos';
import { JwtAuthGuard, Public } from 'src/modules/auth/guards';
import { Message } from 'src/modules/base/dtos';
import { isUUID } from 'class-validator';
import { ControllerInit, ApiInit, UserAuth } from 'src/common/decorators';
import { ErrorRes } from 'src/common/exceptions';
import { catchError } from 'rxjs/operators';

@ControllerInit('user')
@UseGuards(JwtAuthGuard)
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly tool: UserTool,
    private readonly tokenService: TokenService,
  ) {}
  @Post()
  @ApiOperation({ summary: 'create new user' })
  @ApiCreatedResponse({ type: UserResDTO })
  @ApiBadRequestResponse({
    description: 'Email has been exits',
    type: ErrorRes,
  })
  @Public()
  async create(@Body() user: SignUpDTO): Promise<UserResDTO> {
    try {
      const isExits = await this.userService.findByEmail(user.email);
      if (isExits) throw new BadRequestException('Email has been exits!');
      const newUser = await this.userService.newUser(user);
      return this.tool.removeForOne(newUser);
    } catch (error) {
      catchError(error);
    }
  }

  @Post('signin')
  @ApiInit('Sign in', TokenRes, 'Sign in successfully!')
  @ApiBadRequestResponse({
    description: 'Email or password wrong!',
    type: ErrorRes,
  })
  @Public()
  async signIn(
    @Body() body: SignInDTO,
    @Req() req: Request,
  ): Promise<TokenRes> {
    try {
      const user = await this.userService.findByEmail(body.email);
      if (!user) throw new BadRequestException('Email wrong!');
      const isPassword = user.comparePassword(body.password);
      if (!isPassword) throw new BadRequestException('Password wrong!');
      const _id = user._id.toHexString();
      const accessToken = await this.tokenService.createAccessToken(_id);
      const refreshToken = await this.tokenService.createRefreshToken(req, _id);
      return {
        accessToken,
        refreshToken,
      };
    } catch (error) {
      catchError(error);
    }
  }

  @Get()
  @ApiInit('Get user profile', UserResDTO)
  @UserAuth()
  async getProfile(@Id() userId: string): Promise<UserResDTO> {
    try {
      const user = await this.userService.findById(userId);
      return this.tool.removeForOne(user);
    } catch (error) {
      catchError(error);
    }
  }
  @Get('/all')
  @ApiInit('Get all user', UsersResDTO)
  @UserAuth()
  @ApiQuery({
    description: 'Parameter to find ',
    type: FindAll,
    required: false,
  })
  async getAll(@Query() option?: FindAll): Promise<UsersResDTO> {
    try {
      const users = await this.userService.findAllUser(
        undefined,
        Number(option.skip),
        Number(option.limit),
        undefined,
      );
      return this.tool.removeForMany(users);
    } catch (error) {
      catchError(error);
    }
  }

  @Patch('password')
  @ApiInit('Edit password', Message, 'Edit password successfully!')
  @UserAuth()
  async editPassword(
    @Body() edit: ChangerPasswordDTO,
    @Id() userId: string,
  ): Promise<Message> {
    try {
      await this.userService.editPassword(userId, edit.password);
      return { message: 'Changer password successfully!' };
    } catch (error) {
      catchError(error);
    }
  }

  @Patch()
  @ApiInit('Edit user profile', Message, 'Edit profile successfully!')
  @UserAuth()
  async editProfile(
    @Body() edit: UserEditDTO,
    @Id() userId: string,
  ): Promise<Message> {
    try {
      await this.userService.editProfile(userId, edit);
      return { message: 'Edit profile successfully!' };
    } catch (error) {
      catchError(error);
    }
  }

  @Post('logout')
  @ApiInit('Logout or logout all device', Message)
  @UserAuth()
  @ApiBadRequestResponse({ type: ErrorRes })
  async logOut(
    @Id() userId: string,
    @Query() option?: LogOut,
  ): Promise<Message> {
    try {
      if (option.fromAll) {
        await this.tokenService.logoutAll(userId);
      } else {
        if (!option.refreshToken)
          throw new BadRequestException('No refresh token provided');
        await this.tokenService.logout(option.refreshToken);
      }
      return { message: 'Logout successfully!' };
    } catch (error) {
      catchError(error);
    }
  }

  @Get('access-token')
  @ApiInit('Get access token using refresh token', TokenRes)
  @ApiUnauthorizedResponse({ description: 'Token wrong!', type: ErrorRes })
  @Public()
  async getAccessToken(
    @Query('refresh-token') token: string,
  ): Promise<TokenRes> {
    try {
      if (!isUUID(token, 4))
        throw new BadRequestException('Refresh token wrong!');
      const check = await this.tokenService.findRefreshToken(token);
      if (!check) throw new UnauthorizedException('Token wrong!');
      const accessToken = await this.tokenService.createAccessToken(check);
      return {
        accessToken,
        refreshToken: token,
      };
    } catch (error) {
      catchError(error);
    }
  }
}

import { Injectable } from '@nestjs/common';
import { IUserDocument, User } from '../models';
import { SignUpDTO, UserEditDTO } from '../dtos';
import { InjectModel } from '@nestjs/mongoose';
import { catchError } from 'src/common/exceptions';
import { Types } from 'mongoose';
@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: IUserDocument,
  ) {}
  async newUser(user: SignUpDTO): Promise<User> {
    try {
      const newUser = new this.userModel({
        _id: Types.ObjectId(),
        ...user,
      });
      return await newUser.save();
    } catch (error) {
      catchError(error);
    }
  }
  public async findAllUser(
    query?: any,
    skip?: number,
    limit?: number,
    sort?: any,
  ): Promise<User[]> {
    try {
      return await this.userModel
        .find({ query })
        .skip(skip)
        .limit(limit)
        .sort(sort);
    } catch (error) {
      catchError(error);
    }
  }
  public async findByEmail(email: string): Promise<User> {
    try {
      return await this.userModel.findOne({ email });
    } catch (error) {
      catchError(error);
    }
  }
  public async findById(_id: string): Promise<User> {
    try {
      return await this.userModel.findById(_id);
    } catch (error) {
      catchError(error);
    }
  }
  public async editPassword(_id: string, password: string): Promise<User> {
    try {
      const newPassword = await this.userModel.hashPassword(password);
      return await this.userModel.findOneAndUpdate(
        { _id },
        { $set: { password: newPassword } },
      );
    } catch (error) {
      catchError(error);
    }
  }
  public async editProfile(_id: string, update: UserEditDTO): Promise<User> {
    try {
      return await this.userModel.findOneAndUpdate(
        { _id },
        { $set: { ...update } },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }
  public async clearAll(): Promise<any> {
    try {
      return await this.userModel.deleteMany();
    } catch (error) {
      catchError(error);
    }
  }
}

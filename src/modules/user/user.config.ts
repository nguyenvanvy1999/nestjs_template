import { UserSchema, User, UserDocument } from './models';
import bcrypt from 'bcrypt';

export const userMongoConfig = {
  name: User.name,
  useFactory: () => {
    const schema = UserSchema;
    const salt = 10;
    schema.pre(
      'save',
      async function (this: UserDocument, next): Promise<void> {
        try {
          this.password = await bcrypt.hash(this.password, salt);
          next();
        } catch (error) {
          throw error;
        }
      },
    );
    schema.methods.comparePassword = function (
      this: UserDocument,
      password: string,
    ): boolean {
      try {
        return bcrypt.compareSync(password, this.password);
      } catch (error) {
        throw error;
      }
    };
    schema.methods.getFullName = function (this: UserDocument): string {
      try {
        return this.firstName + ' ' + this.lastName;
      } catch (error) {
        throw error;
      }
    };
    schema.statics.hashPassword = async function (
      password: string,
    ): Promise<string> {
      try {
        return await bcrypt.hash(password, salt);
      } catch (error) {
        throw error;
      }
    };
    return schema;
  },
};

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { AreaEnum } from 'src/modules/data/interfaces';
import { NewVisitorDTO } from '../dtos';
import { VisitorDocument, Visitor } from '../models';

@Injectable()
export class VisitorService {
  constructor(
    @InjectModel(Visitor.name)
    private readonly visitorModel: Model<VisitorDocument>,
  ) {}

  public async createVisitor(data: NewVisitorDTO): Promise<Visitor> {
    try {
      const newVisitor = new this.visitorModel({
        _id: Types.ObjectId(),
        ...data,
        timeIn: new Date(data.timeIn),
        timeOut: new Date(data.timeOut),
      });
      await newVisitor.save();
      return newVisitor;
    } catch (error) {
      throw error;
    }
  }

  public async findAll(): Promise<Visitor[]> {
    try {
      return await this.visitorModel.find({}).exec();
    } catch (error) {
      throw error;
    }
  }

  public async findByDay(): Promise<Visitor[]> {
    try {
      return await this.visitorModel.find({}).exec();
    } catch (error) {
      throw error;
    }
  }

  public async findByArea(accessibleArea: AreaEnum): Promise<Visitor[]> {
    try {
      return await this.visitorModel.find({ accessibleArea }).exec();
    } catch (error) {
      throw error;
    }
  }
}

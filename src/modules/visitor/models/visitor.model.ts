import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { schemaOption } from 'src/common/mongo';
import { AreaEnum } from 'src/modules/data/interfaces';

export type VisitorDocument = Visitor & Document;
@Schema(schemaOption)
export class Visitor extends Base {
  @Prop({ type: Date, required: true, default: Date.now() })
  timeIn: Date;

  @Prop({ type: Date, required: true })
  timeOut: Date;

  @Prop({ type: String, required: true })
  visitFor: string;

  @Prop({ type: String, enum: AreaEnum, required: true })
  accessibleArea: AreaEnum;
}

export const VisitorSchema = SchemaFactory.createForClass(Visitor);

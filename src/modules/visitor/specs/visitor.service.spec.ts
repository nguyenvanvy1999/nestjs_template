import { Test, TestingModule } from '@nestjs/testing';
import { VisitorService } from '../services';

describe('VisitorService', () => {
  let visitorService: VisitorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VisitorService],
    }).compile();

    visitorService = module.get<VisitorService>(VisitorService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(visitorService).toBeDefined();
    });
  });
});

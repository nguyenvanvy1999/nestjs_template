import { Test, TestingModule } from '@nestjs/testing';
import { VisitorController } from '../controllers';
import { VisitorService } from '../services';

describe('UserController', () => {
  let visitorController: VisitorController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [VisitorController],
      providers: [VisitorService],
    }).compile();
    visitorController = app.get<VisitorController>(VisitorController);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(visitorController).toBeDefined();
    });
  });
});

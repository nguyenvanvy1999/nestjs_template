import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsEnum } from 'class-validator';
import { string } from 'joi';
import { CheckString } from 'src/common/decorators';
import { AreaEnum } from 'src/modules/data/interfaces';

export class NewVisitorDTO {
  @ApiProperty({
    description: 'Time in',
    required: true,
    type: string,
    format: 'date-time',
  })
  @IsDateString()
  readonly timeIn: string;

  @ApiProperty({
    description: 'Time out',
    required: true,
    type: string,
    format: 'date',
  })
  @IsDateString()
  readonly timeOut: string;

  @ApiProperty({
    description: 'Reason for visit',
    required: true,
    type: String,
  })
  @CheckString()
  readonly visitFor: string;

  @ApiProperty({
    description: 'Area cant visit',
    required: true,
    type: String,
    enum: AreaEnum,
  })
  @IsEnum(AreaEnum)
  readonly accessibleArea: AreaEnum;
}

import { Get, Query } from '@nestjs/common';
import { ApiInit, ControllerInit } from 'src/common/decorators';
import { AreaEnum } from 'src/modules/data/interfaces';
import { Visitor } from '../models';
import { VisitorService } from '../services';

@ControllerInit('visitor')
export class VisitorController {
  constructor(private readonly visitorService: VisitorService) {}
  @Get('all')
  @ApiInit('Get all visitor', [Visitor])
  public async getAllVisitor(): Promise<Visitor[]> {
    try {
      return await this.visitorService.findAll();
    } catch (error) {
      throw error;
    }
  }
  @Get('in-day')
  @ApiInit('Get visitor in day', [Visitor])
  public async getVisitorInDay(@Query('day') day: string): Promise<Visitor[]> {
    try {
      return await this.visitorService.findByDay();
    } catch (error) {
      throw error;
    }
  }

  @Get('by-area')
  @ApiInit('Get visitor by area', [Visitor])
  public async getVisitorByArea(
    @Query('area') area: AreaEnum,
  ): Promise<Visitor[]> {
    try {
      return await this.visitorService.findByArea(area);
    } catch (error) {
      throw error;
    }
  }
}

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types, Schema as MongooseSchema } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { schemaOption } from 'src/common/mongo';

export type ZoneDocument = Zone & Document;
@Schema(schemaOption)
export class Zone extends Base {
  @Prop({ type: String, required: true })
  zoneName: string;

  @Prop({ type: [{ type: MongooseSchema.Types.ObjectId, ref: 'Device' }] })
  deviceIds: Types.ObjectId[];

  @Prop({ type: Number, required: true, default: 1 })
  deviceCount: number;
}

export const ZoneSchema = SchemaFactory.createForClass(Zone);

import { Test, TestingModule } from '@nestjs/testing';
import { ZoneController } from '../controllers/zone.controller';
import { ZoneService } from '../services';

describe('ZoneController', () => {
  let zoneController: ZoneController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ZoneController],
      providers: [ZoneService],
    }).compile();
    zoneController = app.get<ZoneController>(ZoneController);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(zoneController).toBeDefined();
    });
  });
});

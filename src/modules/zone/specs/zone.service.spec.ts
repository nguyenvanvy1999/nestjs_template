import { Test, TestingModule } from '@nestjs/testing';
import { ZoneService } from '../services';

describe('ZoneService', () => {
  let zoneService: ZoneService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ZoneService],
    }).compile();
    zoneService = module.get<ZoneService>(ZoneService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(zoneService).toBeDefined();
    });
  });
});

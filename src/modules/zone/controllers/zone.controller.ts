import { Body, Get, Patch, Post, Query } from '@nestjs/common';
import { ApiCreatedResponse, ApiOperation } from '@nestjs/swagger';
import { catchError } from 'rxjs/operators';
import { ControllerInit, ApiInit } from 'src/common/decorators';
import { Pagination } from 'src/modules/base/dtos';
import { ChangerDevicesDTO, NewZoneDTO, EditZoneDTO } from '../dtos';
import { Zone } from '../models';
import { ZoneService } from '../services';

@ControllerInit('zone')
export class ZoneController {
  constructor(private readonly zoneService: ZoneService) {}
  @Post()
  @ApiOperation({ summary: 'Create new zone' })
  @ApiCreatedResponse({ description: 'OK', type: Zone })
  public async newZone(@Body() body: NewZoneDTO): Promise<Zone> {
    try {
      return await this.zoneService.newZone(body);
    } catch (error) {
      catchError(error);
    }
  }

  @Patch('add')
  @ApiInit('Add devices to zone', Zone)
  public async addDevices(@Body() body: ChangerDevicesDTO): Promise<Zone> {
    try {
      return await this.zoneService.addDevices(body);
    } catch (error) {
      catchError(error);
    }
  }

  @Patch('rmv')
  @ApiInit('Remove devices', Zone)
  public async rmvDevices(@Body() body: ChangerDevicesDTO): Promise<Zone> {
    try {
      return await this.zoneService.rmvDevices(body);
    } catch (error) {
      catchError(error);
    }
  }

  @Patch()
  @ApiInit('Edit zone', Zone)
  public async editZone(@Body() body: EditZoneDTO): Promise<Zone> {
    try {
      return await this.zoneService.editZone(body);
    } catch (error) {
      catchError(error);
    }
  }
  @Get()
  @ApiInit('Get all zone', [Zone])
  public async findAll(@Query() pagination?: Pagination): Promise<Zone[]> {
    try {
      return await this.zoneService.getAllZone(pagination);
    } catch (error) {
      catchError(error);
    }
  }

  @Get('/:id')
  @ApiInit('Get zone by id', Zone)
  public async findById(@Query() id: string): Promise<Zone> {
    try {
      return await this.zoneService.getById(id);
    } catch (error) {
      catchError(error);
    }
  }
  @Get('/device/:id')
  @ApiInit('Get all device in zone', [String])
  public async findDevicesOfZone(@Query() id: string): Promise<string[]> {
    try {
      return await this.zoneService.getAllDeviceZone(id);
    } catch (error) {
      catchError(error);
    }
  }
}

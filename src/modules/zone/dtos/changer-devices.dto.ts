import { ApiProperty } from '@nestjs/swagger';
import { ArrayMongoId, MongoId } from 'src/common/decorators';

export class ChangerDevicesDTO {
  @ApiProperty({
    description: 'ZoneId to changer',
    required: true,
    type: String,
  })
  @MongoId(true)
  zoneId: string;

  @ApiProperty({
    description: 'DeviceIds to changer',
    type: [String],
    required: true,
    minItems: 1,
  })
  @ArrayMongoId(1)
  deviceIds: string[];
}

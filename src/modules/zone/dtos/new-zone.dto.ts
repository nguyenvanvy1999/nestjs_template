import { ApiProperty } from '@nestjs/swagger';
import { ArrayMongoId, CheckString } from 'src/common/decorators';

export class NewZoneDTO {
  @ApiProperty({ description: 'Name of zone', type: String })
  @CheckString()
  readonly zoneName: string;

  @ApiProperty({
    description: 'DeviceIds to add',
    required: true,
    type: [String],
    minItems: 1,
  })
  @ArrayMongoId(1)
  deviceIds: string[];
}

import { ApiProperty } from '@nestjs/swagger';
import { CheckString, MongoId } from 'src/common/decorators';

export class EditZoneDTO {
  @ApiProperty({ description: 'ZoneId to edit', type: String, required: true })
  @MongoId(true)
  readonly zoneId: string;

  @ApiProperty({ description: 'Name of zone', type: String, required: true })
  @CheckString()
  readonly zoneName: string;
}

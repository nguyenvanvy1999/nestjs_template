import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { catchError } from 'rxjs/operators';
import { Pagination } from 'src/modules/base/dtos';
import { ChangerDevicesDTO, NewZoneDTO, EditZoneDTO } from '../dtos';
import { Zone, ZoneDocument } from '../models';

@Injectable()
export class ZoneService {
  constructor(
    @InjectModel(Zone.name) private readonly zoneModel: Model<ZoneDocument>,
  ) {}

  public async newZone(data: NewZoneDTO): Promise<Zone> {
    try {
      const newZone = new this.zoneModel({
        _id: Types.ObjectId(),
        deviceIds: data.deviceIds,
        count: data.deviceIds.length,
      });
      await newZone.save();
      return newZone;
    } catch (error) {
      catchError(error);
    }
  }
  public async addDevices(data: ChangerDevicesDTO): Promise<Zone> {
    try {
      return await this.zoneModel.findOneAndUpdate(
        {
          _id: data.zoneId,
        },
        {
          $push: { $each: { deviceIds: data.deviceIds } },
          $inc: { deviceCount: data.deviceIds.length },
        },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }

  public async rmvDevices(data: ChangerDevicesDTO): Promise<Zone> {
    try {
      return await this.zoneModel.findOneAndUpdate(
        { _id: data.zoneId },
        {
          $pullAll: { deviceIds: data.deviceIds },
          $inc: { deviceCount: -data.deviceIds.length },
        },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }

  public async editZone(data: EditZoneDTO): Promise<Zone> {
    try {
      return await this.zoneModel.findOneAndUpdate(
        {
          _id: data.zoneId,
        },
        { $set: { zoneName: data.zoneName } },
      );
    } catch (error) {
      catchError(error);
    }
  }
  public async getAllDeviceZone(zoneId: string): Promise<string[]> {
    try {
      const zone = await this.zoneModel.findById(zoneId);
      const { deviceIds } = zone;
      return deviceIds.map((deviceId) => deviceId.toHexString());
    } catch (error) {
      catchError(error);
    }
  }
  public async getAllZone(pagination?: Pagination): Promise<Zone[]> {
    try {
      return await this.zoneModel
        .find()
        .skip(pagination?.skip)
        .limit(pagination?.limit)
        .lean();
    } catch (error) {
      catchError(error);
    }
  }
  public async getById(id: string): Promise<Zone> {
    try {
      return await this.zoneModel.findById(id);
    } catch (error) {
      catchError(error);
    }
  }
  public async clearAll(): Promise<any> {
    try {
      return await this.zoneModel.deleteMany();
    } catch (error) {
      catchError(error);
    }
  }
}

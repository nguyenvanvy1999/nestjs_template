import { ApiProperty } from '@nestjs/swagger';

export class TokenRes {
  @ApiProperty({
    type: String,
    required: true,
    readOnly: true,
    description: 'Access token',
  })
  accessToken: string;
  @ApiProperty({
    type: String,
    required: true,
    readOnly: true,
    description: 'Refresh token',
  })
  refreshToken: string;
}

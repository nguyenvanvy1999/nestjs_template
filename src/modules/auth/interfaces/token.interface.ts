import { BaseInterface } from 'src/modules/base/interfaces';

export interface TokenInterface extends BaseInterface {
  user: string;
  value: string;
  ip?: string;
  browser?: string;
  country?: string;
}

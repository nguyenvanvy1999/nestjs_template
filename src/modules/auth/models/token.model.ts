import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types, Schema as MongooseSchema } from 'mongoose';
import { schemaOption } from 'src/common/mongo';
import { Base } from 'src/modules/base/models';

export type TokenDocument = Token & Document;

@Schema(schemaOption)
export class Token extends Base {
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    required: true,
    ref: 'User',
    immutable: true,
  })
  userId: Types.ObjectId;
  @Prop({ required: true, immutable: true }) value: string;
  @Prop({ required: true, immutable: true }) ip: string;
  @Prop({ required: true, immutable: true }) browser: string;
  @Prop({ required: true, immutable: true }) country: string;
}

export const TokenSchema = SchemaFactory.createForClass(Token);

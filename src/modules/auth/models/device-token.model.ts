import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types, Schema as MongooseSchema } from 'mongoose';
import { schemaOption } from 'src/common/mongo';
import { Base } from 'src/modules/base/models';

export type DeviceTokenDocument = DeviceToken & Document;

@Schema(schemaOption)
export class DeviceToken extends Base {
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    required: true,
    ref: 'Device',
    immutable: true,
  })
  deviceId: Types.ObjectId;
  @Prop({ required: true, immutable: true }) token: string;
  @Prop({ required: true, immutable: true }) IPAddress: string;
  @Prop({ immutable: true }) deviceMac: string;
}

export const DeviceTokenSchema = SchemaFactory.createForClass(DeviceToken);

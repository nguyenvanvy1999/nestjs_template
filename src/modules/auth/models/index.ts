export { Token, TokenDocument, TokenSchema } from './token.model';
export {
  DeviceToken,
  DeviceTokenDocument,
  DeviceTokenSchema,
} from './device-token.model';

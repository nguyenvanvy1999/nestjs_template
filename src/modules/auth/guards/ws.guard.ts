import { Injectable, CanActivate } from '@nestjs/common';
import { Observable } from 'rxjs';
import jwt from 'jsonwebtoken';
import { AppConfigService } from 'src/common/env';
import { DeviceService } from 'src/modules/device/services';

@Injectable()
export class WsGuard implements CanActivate {
  constructor(
    private deviceService: DeviceService,
    private readonly config: AppConfigService,
  ) {}

  canActivate(
    context: any,
  ): boolean | any | Promise<boolean | any> | Observable<boolean | any> {
    const bearerToken =
      context.args[0].handshake.headers.authorization.split(' ')[1];
    try {
      const decoded = jwt.verify(
        bearerToken,
        this.config.get<string>('DEVICE_SECRET'),
      ) as any;
      return new Promise((resolve, reject) => {
        return this.deviceService.findById(decoded.device).then((device) => {
          if (device) {
            resolve(device);
          } else {
            reject(false);
          }
        });
      });
    } catch (ex) {
      console.log(ex);
      return false;
    }
  }
}

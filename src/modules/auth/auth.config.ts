import { AppConfigModule, AppConfigService } from 'src/common/env';

export const jwtConfig = {
  imports: [AppConfigModule],
  useFactory: (config: AppConfigService) => {
    return {
      secret: config.get<string>('JWT_SECRET'),
      signOptions: { expiresIn: '15m' },
    };
  },
  inject: [AppConfigService],
};

export const jwtDeviceConfig = {
  imports: [AppConfigModule],
  useFactory: (config: AppConfigService) => {
    return {
      secret: config.get<string>('DEVICE_SECRET'),
      signOptions: { expiresIn: '1h' },
    };
  },
  inject: [AppConfigService],
};

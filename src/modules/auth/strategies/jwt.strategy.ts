import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AppConfigService } from 'src/common/env';
import { catchError } from 'src/common/exceptions';
import { UserService } from 'src/modules/user/services';
import { Payload } from '../interfaces';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'user_jwt') {
  constructor(
    private readonly userService: UserService,
    private readonly config: AppConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        ExtractJwt.fromHeader('token'),
      ]),
      ignoreExpiration: false,
      secretOrKey: config.get<string>('JWT_SECRET'),
    });
  }

  async validate(payload: Payload): Promise<any> {
    try {
      const user = await this.userService.findById(payload.user);
      if (!user) throw new UnauthorizedException('Unauthorized!');
      return { _id: user._id };
    } catch (error) {
      catchError(error);
    }
  }
}

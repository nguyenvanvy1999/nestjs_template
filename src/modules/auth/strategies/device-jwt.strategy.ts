import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { catchError } from 'src/common/exceptions';
import { DeviceService } from 'src/modules/device/services';
import { DevicePayload } from '../interfaces';

@Injectable()
export class DeviceJwtStrategy extends PassportStrategy(
  Strategy,
  'device_jwt',
) {
  constructor(
    private readonly config: ConfigService,
    private readonly deviceService: DeviceService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        ExtractJwt.fromHeader('token'),
      ]),
      ignoreExpiration: false,
      secretOrKey: config.get<string>('DEVICE_SECRET'),
    });
  }

  async validate(payload: DevicePayload) {
    try {
      const device = await this.deviceService.findById(payload.device);
      if (!device) throw new UnauthorizedException('Unauthorized !');
      return { _id: device._id };
    } catch (error) {
      catchError(error);
    }
  }
}

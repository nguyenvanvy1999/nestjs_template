import { Model, Types } from 'mongoose';
import { Token, TokenDocument } from '../models';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { JwtService } from '@nestjs/jwt';
import { AppConfigService } from 'src/common/env';
import { v4 } from 'uuid';
import { Request } from 'express';
import { getClientIp } from 'request-ip';
import geoIp from 'geoip-country';
import { catchError } from 'src/common/exceptions';

@Injectable()
export class TokenService {
  constructor(
    @InjectModel(Token.name)
    private readonly tokenModel: Model<TokenDocument>,
    private readonly jwtService: JwtService,
    private readonly config: AppConfigService,
  ) {}
  public async createAccessToken(user: string): Promise<string> {
    try {
      const token = await this.jwtService.signAsync(
        { user },
        {
          secret: this.config.get<string>('JWT_SECRET'),
          expiresIn: this.config.get<string>('JWT_EXPIRATION'),
        },
      );
      return token;
    } catch (error) {
      catchError(error);
    }
  }
  public async createRefreshToken(req: Request, user: string): Promise<string> {
    try {
      const refreshToken = new this.tokenModel({
        _id: Types.ObjectId(),
        user,
        value: v4(),
        ip: this.getIp(req),
        browser: this.getBrowserInfo(req),
        country: this.getCountry(req),
      });
      await refreshToken.save();
      return refreshToken.value;
    } catch (error) {
      catchError(error);
    }
  }
  public async findRefreshToken(token: string): Promise<string> {
    try {
      const refreshToken = await this.tokenModel.findOne({ value: token });
      if (!refreshToken)
        throw new UnauthorizedException('User has been logged out.');
      return refreshToken.userId.toHexString();
    } catch (error) {
      catchError(error);
    }
  }
  public async logout(value: string): Promise<Token> {
    try {
      return await this.tokenModel.findOneAndDelete({ value });
    } catch (error) {
      catchError(error);
    }
  }
  public async clearAll(): Promise<any> {
    try {
      return await this.tokenModel.deleteMany();
    } catch (error) {
      catchError(error);
    }
  }
  public async logoutAll(userId: string): Promise<any> {
    try {
      return await this.tokenModel.deleteMany({ userId });
    } catch (error) {
      catchError(error);
    }
  }
  private getIp(req: Request): string {
    return getClientIp(req);
  }
  private getBrowserInfo(req: Request): string {
    return req.headers['user-agent'] || 'XX';
  }
  private getCountry(req: Request): string {
    return geoIp.lookup(getClientIp(req))
      ? geoIp.lookup(getClientIp(req)).country
      : 'XX';
  }
}

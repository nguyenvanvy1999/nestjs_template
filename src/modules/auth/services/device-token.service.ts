import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { AppConfigService } from 'src/common/env';
import { v4 } from 'uuid';
import { DeviceToken, DeviceTokenDocument } from '../models';
import { getClientIp } from 'request-ip';
import { Request } from 'express';
import { catchError } from 'src/common/exceptions';

@Injectable()
export class DeviceTokenService {
  constructor(
    @InjectModel(DeviceToken.name)
    private readonly tokenModel: Model<DeviceTokenDocument>,
    private readonly config: AppConfigService,
    private readonly jwtService: JwtService,
  ) {}

  public async createAccessToken(deviceId: string): Promise<string> {
    try {
      const token = await this.jwtService.signAsync(
        { device: deviceId },
        {
          secret: this.config.get<string>('DEVICE_SECRET'),
          expiresIn: '1h',
        },
      );
      return token;
    } catch (error) {
      catchError(error);
    }
  }
  public async createRefreshToken(
    req: Request,
    deviceId: string,
  ): Promise<string> {
    try {
      const refreshToken = new this.tokenModel({
        _id: Types.ObjectId(),
        device: deviceId,
        token: v4(),
        IPAddress: this.getIp(req),
      });
      await refreshToken.save();
      return refreshToken.token;
    } catch (error) {
      catchError(error);
    }
  }
  public async findRefreshTokenByToken(token: string): Promise<string> {
    try {
      const refreshToken = await this.tokenModel.findOne({ token });
      if (!refreshToken)
        throw new UnauthorizedException('No device token found!');
      return refreshToken.deviceId.toHexString();
    } catch (error) {
      catchError(error);
    }
  }
  public async findRefreshTokenByDevice(deviceId: string): Promise<string> {
    try {
      const refreshToken = await this.tokenModel.findOne({
        device: deviceId,
      });
      if (!refreshToken)
        throw new UnauthorizedException('No device token found!');
      return refreshToken.deviceId.toHexString();
    } catch (error) {
      catchError(error);
    }
  }
  public async logout(device: string): Promise<DeviceToken> {
    try {
      return await this.tokenModel.findOneAndDelete({ device });
    } catch (error) {
      catchError(error);
    }
  }
  public async clearAll(): Promise<any> {
    try {
      return await this.tokenModel.deleteMany();
    } catch (error) {
      catchError(error);
    }
  }
  private getIp(req: Request): string {
    return getClientIp(req);
  }
}

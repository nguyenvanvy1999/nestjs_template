import {
  Post,
  Body,
  Query,
  Delete,
  Get,
  Patch,
  UploadedFiles,
  BadRequestException,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { catchError, ErrorRes } from 'src/common/exceptions';
import {
  UploadMultiAndBody,
  ControllerInit,
  DeviceAuth,
  ApiInit,
} from 'src/common/decorators';
import { Id } from 'src/modules/auth/decorators';
import { ImageService } from 'src/modules/image/services';
import {
  EditImagesDTO,
  NewPersonDTO,
  RemoveImageDTO,
  UpdatePersonDTO,
} from '../dtos';
import { Person } from '../models';
import { PersonService } from '../services';
import { ObjectIds, removeFiles } from 'src/modules/base/tools';
import { Message } from 'src/modules/base/dtos';
import { isContains } from '../tools';

@ControllerInit('person')
@DeviceAuth()
export class PersonController {
  constructor(
    private readonly personService: PersonService,
    private readonly imageService: ImageService,
  ) {}
  @Post()
  @ApiOperation({ summary: 'Create new person' })
  @UploadMultiAndBody('files', 8, NewPersonDTO)
  @ApiCreatedResponse({ type: Person })
  public async newPerson(
    @UploadedFiles() files: Express.Multer.File[],
    @Body() body: NewPersonDTO,
    @Id() id: string,
  ): Promise<Person> {
    try {
      const person = await this.personService.newPerson({
        ...body,
        imageIds: null,
        infoId: '609f7e2c826ffd3dec4c8f54',
      });
      const ids: string[] = [];
      await Promise.all(
        files.map(async (file) => {
          const image = await this.imageService.newImage({
            name: file.filename,
            deviceId: id,
            personId: person._id.toHexString(),
            path: file.path,
          });
          ids.push(image._id.toHexString());
        }),
      );
      person.imageIds = ObjectIds(ids);
      await person.save();
      return person;
    } catch (error) {
      removeFiles(files);
      catchError(error);
    }
  }

  @Delete()
  @ApiInit('Delete person', Message)
  public async deletePerson(@Query('id') personId: string): Promise<Message> {
    try {
      const person = await this.personService.findById(personId);
      if (!person) throw new BadRequestException('PersonId wrong!');
      const imageIds = await this.personService.getAllImagesOfPerson(personId);
      await this.imageService.removeImagesByIds(imageIds);
      await this.personService.deletePerson(personId);
      return { message: 'Delete successfully!' };
    } catch (error) {
      catchError(error);
    }
  }

  @Get()
  @ApiInit('Get person information', Person)
  public async getPersonInfo(@Query('id') personId: string): Promise<Person> {
    try {
      return await this.personService.findById(personId);
    } catch (error) {
      catchError(error);
    }
  }

  @Patch()
  @ApiInit('Edit person information', Person)
  @ApiBadRequestResponse({ description: 'PersonId wrong!', type: ErrorRes })
  public async editPerson(@Body() body: UpdatePersonDTO): Promise<Person> {
    try {
      const person = await this.personService.findById(body.personId);
      if (!person) throw new BadRequestException('PersonId wrong!');
      return await this.personService.editPerson(body.personId, body);
    } catch (error) {
      catchError(error);
    }
  }

  @Patch('images')
  @ApiInit('Add images to person', Person)
  @UploadMultiAndBody('files', 8, EditImagesDTO)
  @ApiBadRequestResponse({ description: 'PersonId wrong!', type: ErrorRes })
  public async addImages(
    @UploadedFiles() files: Express.Multer.File[],
    @Body() body: EditImagesDTO,
    @Id() id: string,
  ): Promise<Person> {
    try {
      const person = await this.personService.findById(body.personId);
      if (!person) throw new BadRequestException('PersonId wrong !');
      const ids: string[] = [];
      await Promise.all(
        files.map(async (file) => {
          const image = await this.imageService.newImage({
            name: file.filename,
            deviceId: id,
            personId: body.personId,
            path: file.path,
          });
          ids.push(image._id.toHexString());
        }),
      );
      return await this.personService.addImages(body.personId, ids);
    } catch (error) {
      removeFiles(files);
      catchError(error);
    }
  }

  @Patch('remove-images')
  @ApiInit('Remove images from person', Person)
  @ApiBadRequestResponse({ description: 'ImageIds wrong!', type: ErrorRes })
  public async removerImages(@Body() body: RemoveImageDTO): Promise<Person> {
    try {
      const { personId, imageIds } = body;
      const images = await this.personService.getAllImagesOfPerson(personId);
      if (!isContains(body.imageIds, images))
        throw new BadRequestException('ImageIds wrong!');
      await this.imageService.removeImagesByIds(imageIds);
      return await this.personService.removeImages(body);
    } catch (error) {
      catchError(error);
    }
  }
}

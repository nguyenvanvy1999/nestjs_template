import { ApiProperty, OmitType } from '@nestjs/swagger';
import { NewPerson } from './new-person.dto';

export class NewPersonDTO extends OmitType(NewPerson, ['infoId', 'imageIds']) {
  @ApiProperty({ type: [String], format: 'binary' })
  readonly files: any[];

  imageIds?: string[];
}

export { NewPerson } from './new-person.dto';
export { NewPersonDTO } from './create-person.dto';
export { EditImagesDTO } from './edit-images.dto';
export { RemoveImageDTO } from './remove-image.dto';
export { UpdatePersonDTO } from './update-person.dto';

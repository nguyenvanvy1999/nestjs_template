import { ApiProperty } from '@nestjs/swagger';
import { MongoId } from 'src/common/decorators';

export class EditImagesDTO {
  @ApiProperty({
    description: 'Person id to change',
    type: String,
    required: true,
  })
  @MongoId(true)
  readonly personId: string;

  @ApiProperty({ type: [String], format: 'binary' })
  readonly files: any[];
}

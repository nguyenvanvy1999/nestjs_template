import { ApiProperty } from '@nestjs/swagger';
import { ArrayMongoId, MongoId } from 'src/common/decorators';

export class RemoveImageDTO {
  @ApiProperty({ description: 'PersonId', required: true, type: String })
  @MongoId(true)
  readonly personId: string;

  @ApiProperty({
    description: 'ImageIds to remove',
    required: true,
    type: [String],
  })
  @ArrayMongoId(1)
  readonly imageIds: string[];
}

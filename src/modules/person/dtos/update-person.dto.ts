import { ApiProperty, OmitType } from '@nestjs/swagger';
import { ArrayMongoId } from 'src/common/decorators';
import { NewPerson } from './new-person.dto';

export class UpdatePersonDTO extends OmitType(NewPerson, [
  'infoId',
  'personType',
  'imageIds',
]) {
  @ApiProperty({ description: 'PersonId', required: true })
  @ArrayMongoId(1)
  personId: string;
}

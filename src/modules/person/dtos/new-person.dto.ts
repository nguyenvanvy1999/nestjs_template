import { ApiProperty } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';
import { ArrayMongoId, CheckString, MongoId } from 'src/common/decorators';
import { Gender } from 'src/modules/user/interfaces';
import { CertificateType, PersonType } from '../interfaces';

export class NewPerson {
  @ApiProperty({ description: 'Name of person', type: String, required: true })
  @CheckString()
  readonly name: string;

  @ArrayMongoId(1)
  readonly imageIds: string[];

  @ApiProperty({
    description: 'Gender',
    type: String,
    enum: Gender,
    default: Gender.Undisclosed,
    required: true,
  })
  @IsEnum(Gender)
  readonly gender: Gender;

  @ApiProperty({
    description: 'Certificate type',
    type: String,
    required: true,
    enum: CertificateType,
    default: CertificateType.IDCard,
  })
  @IsEnum(CertificateType)
  readonly cerType: CertificateType;

  @ApiProperty({ description: 'Certificate No', type: String, required: true })
  @CheckString()
  readonly cerNo: string;

  @ApiProperty({
    description: 'Person type',
    type: String,
    required: true,
    enum: PersonType,
    default: PersonType.Unknown,
  })
  @IsEnum(PersonType)
  readonly personType: PersonType;

  @MongoId(true)
  readonly infoId: string;
}

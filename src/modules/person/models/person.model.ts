import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types, Schema as MongooseSchema } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { schemaOption } from 'src/common/mongo';
import { Gender } from 'src/modules/user/interfaces';
import { CertificateType, PersonType } from '../interfaces';

export type PersonDocument = Person & Document;
@Schema(schemaOption)
export class Person extends Base {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: [{ type: MongooseSchema.Types.ObjectId, ref: 'Image' }] })
  imageIds: Types.ObjectId[];

  @Prop({
    type: String,
    enum: Object.values(Gender),
    required: true,
    default: Gender.Undisclosed,
  })
  gender: Gender;

  @Prop({
    type: String,
    enum: Object.values(CertificateType),
    required: true,
    default: CertificateType.IDCard,
  })
  cerType: CertificateType;

  @Prop({ type: String, required: true })
  cerNo: string;

  @Prop({
    type: String,
    required: true,
    enum: Object.values(PersonType),
    default: PersonType.Unknown,
  })
  personType: PersonType;

  @Prop({ type: MongooseSchema.Types.ObjectId, required: true })
  infoId: Types.ObjectId;
}

export const PersonSchema = SchemaFactory.createForClass(Person);

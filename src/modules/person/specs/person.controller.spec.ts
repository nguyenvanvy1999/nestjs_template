import { Test, TestingModule } from '@nestjs/testing';
import { ImageService } from 'src/modules/image/services';
import { PersonController } from '../controllers';
import { PersonService } from '../services';

describe('PersonController', () => {
  let personController: PersonController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [PersonController],
      providers: [PersonService, ImageService],
    }).compile();
    personController = app.get<PersonController>(PersonController);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(personController).toBeDefined();
    });
  });
});

import { Test, TestingModule } from '@nestjs/testing';
import { PersonService } from '../services';

describe('PersonService', () => {
  let personService: PersonService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PersonService],
    }).compile();
    personService = module.get<PersonService>(PersonService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(personService).toBeDefined();
    });
  });
});

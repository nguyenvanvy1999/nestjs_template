export const isContains = (ar1: any[], ar2: any[]): boolean => {
  return ar1.every((r) => ar2.includes(r));
};

import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { jwtDeviceConfig } from '../auth/auth.config';
import { DeviceJwtStrategy } from '../auth/strategies';
import { DeviceModule } from '../device/device.module';
import { ImageModule } from '../image/image.module';
import { PersonController } from './controllers';
import { Person, PersonSchema } from './models';
import { PersonService } from './services';

@Module({
  imports: [
    ImageModule,
    DeviceModule,
    PassportModule,
    JwtModule.registerAsync(jwtDeviceConfig),
    MongooseModule.forFeature([{ name: Person.name, schema: PersonSchema }]),
  ],
  controllers: [PersonController],
  providers: [PersonService, DeviceJwtStrategy],
  exports: [PersonService],
})
export class PersonModule {}

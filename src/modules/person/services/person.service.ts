import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { catchError } from 'src/common/exceptions';
import { NewPerson, RemoveImageDTO, UpdatePersonDTO } from '../dtos';
import { Person, PersonDocument } from '../models';

@Injectable()
export class PersonService {
  constructor(
    @InjectModel(Person.name)
    private readonly personModel: Model<PersonDocument>,
  ) {}

  public async newPerson(data: NewPerson): Promise<Person> {
    try {
      const person = new this.personModel({
        _id: Types.ObjectId(),
        ...data,
        imageIds: data.imageIds,
        infoId: data.infoId,
      });
      await person.save();
      return person;
    } catch (error) {
      catchError(error);
    }
  }

  public async findById(id: string): Promise<Person> {
    try {
      return await this.personModel.findById(id);
    } catch (error) {
      catchError(error);
    }
  }

  public async deletePerson(personId: string): Promise<Person> {
    try {
      return await this.personModel.findOneAndDelete({ _id: personId });
    } catch (error) {
      catchError(error);
    }
  }
  public async editPerson(
    personId: string,
    update: UpdatePersonDTO,
  ): Promise<Person> {
    try {
      return await this.personModel.findOneAndUpdate(
        { _id: personId },
        { $set: { ...update } },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }
  public async addImages(
    personId: string,
    imageIds: string[],
  ): Promise<Person> {
    try {
      return await this.personModel.findOneAndUpdate(
        {
          _id: personId,
        },
        {
          $push: { imageIds: { $each: imageIds } },
        },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }

  public async removeImages(data: RemoveImageDTO): Promise<Person> {
    try {
      return await this.personModel.findOneAndUpdate(
        {
          _id: data.personId,
        },
        { $pullAll: { imageIds: data.imageIds } },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }

  public async getAllImagesOfPerson(personId: string): Promise<string[]> {
    try {
      const person = await this.personModel.findById(personId);
      const { imageIds } = person;
      const ids: string[] = [];
      imageIds.forEach((id) => ids.push(id.toHexString()));
      return ids;
    } catch (error) {
      catchError(error);
    }
  }
  public async clearAll(): Promise<any> {
    try {
      return await this.personModel.deleteMany();
    } catch (error) {
      catchError(error);
    }
  }
}

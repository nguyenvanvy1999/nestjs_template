export enum PersonType {
  Visitor = 'Visitor',
  Customer = 'Customer',
  Employee = 'Employee',
  Unknown = 'Unknown',
}

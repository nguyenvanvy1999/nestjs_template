export enum CertificateType {
  IDCard = 'Chung minh nhan dan/ Can cuoc cong dan',
  DriverLic = 'Giay phep lai xe',
  ResidencePer = 'So ho khau/ Dang ky tam tru',
  Passport = 'Passport',
  EEP = 'Exit-Entry Permit(EEP) for HK/Macau',
}

import { Test, TestingModule } from '@nestjs/testing';
import { DataService } from '../services';

describe('DataService', () => {
  let dataService: DataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DataService],
    }).compile();
    dataService = module.get<DataService>(DataService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(dataService).toBeDefined();
    });
  });
});

import { Test, TestingModule } from '@nestjs/testing';
import { DataController } from '../controllers';
import { DataService } from '../services';

describe('DataController', () => {
  let dataController: DataController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [DataController],
      providers: [DataService],
    }).compile();
    dataController = app.get<DataController>(DataController);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(dataController).toBeDefined();
    });
  });
});

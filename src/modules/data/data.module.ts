import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { jwtDeviceConfig } from '../auth/auth.config';
import { DeviceJwtStrategy } from '../auth/strategies';
import { DeviceModule } from '../device/device.module';
import { ImageModule } from '../image/image.module';
import { PersonModule } from '../person/person.module';
import { DataController } from './controllers';
import { Data, DataSchema } from './models';
import { DataService } from './services';

@Module({
  imports: [
    ImageModule,
    DeviceModule,
    PersonModule,
    PassportModule,
    JwtModule.registerAsync(jwtDeviceConfig),
    MongooseModule.forFeature([{ name: Data.name, schema: DataSchema }]),
  ],
  controllers: [DataController],
  providers: [DataService, DeviceJwtStrategy],
  exports: [DataService],
})
export class DataModule {}

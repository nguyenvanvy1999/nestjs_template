import { ApiProperty, OmitType } from '@nestjs/swagger';
import { DataDTO } from './data.dto';

export class DataCreateDTO extends OmitType(DataDTO, ['imageId']) {
  @ApiProperty({ type: [String], format: 'binary' })
  readonly file: any[];
}

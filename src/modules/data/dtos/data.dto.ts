import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsBoolean, IsDateString, IsEnum, IsNotEmpty } from 'class-validator';
import { MongoId } from 'src/common/decorators';
import { AreaEnum } from '../interfaces';

export class DataDTO {
  @ApiProperty({ description: 'PersonId', type: String, required: true })
  @MongoId(true)
  personId: string;

  @ApiProperty({
    description: 'Time of create',
    type: String,
    format: 'date-time',
    required: true,
  })
  @IsDateString()
  time: Date;

  @MongoId(true)
  imageId: string;

  @ApiProperty({ description: 'Is mask ?', type: Boolean, default: false })
  @IsBoolean()
  @IsNotEmpty()
  @Type(() => Boolean)
  mask: boolean;

  @ApiProperty({
    description: 'Area',
    type: String,
    enum: AreaEnum,
    default: AreaEnum.Pool,
  })
  @IsEnum(AreaEnum)
  @IsNotEmpty()
  area: string;
}

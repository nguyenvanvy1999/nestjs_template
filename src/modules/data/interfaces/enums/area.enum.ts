export enum AreaEnum {
  Buffet = 'Buffet',
  Pool = 'Pool',
  Reception = 'Reception',
}

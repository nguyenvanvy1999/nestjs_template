import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { catchError } from 'src/common/exceptions';
import { Pagination } from 'src/modules/base/dtos';
import { DataDTO } from '../dtos';
import { Data, DataDocument } from '../models';

@Injectable()
export class DataService {
  constructor(
    @InjectModel(Data.name) private readonly dataModel: Model<DataDocument>,
  ) {}

  public async newData(data: DataDTO): Promise<Data> {
    try {
      const newData = new this.dataModel({
        _id: Types.ObjectId(),
        ...data,
        personId: data.personId === 'Unknown' ? null : data.personId,
        imageId: data.imageId,
        time: new Date(data.time),
      });
      await newData.save();
      return newData;
    } catch (error) {
      catchError(error);
    }
  }
  public async findAll(pagination?: Pagination): Promise<Data[]> {
    try {
      return await this.dataModel
        .find()
        .limit(pagination?.limit)
        .skip(pagination?.skip)
        .populate('personId')
        .populate('imageId')
        .lean();
    } catch (error) {
      catchError(error);
    }
  }
  public async findById(id: string): Promise<Data> {
    try {
      return await this.dataModel
        .findById(id)
        .populate('personId')
        .populate('imageId');
    } catch (error) {
      catchError(error);
    }
  }
  public async findAllDataOfPerson(
    personId: string,
    pagination?: Pagination,
  ): Promise<Data[]> {
    try {
      return await this.dataModel
        .find({ personId })
        .skip(pagination?.skip)
        .limit(pagination?.limit)
        .lean();
    } catch (error) {
      catchError(error);
    }
  }
  public async findAllDataUnknown(pagination?: Pagination): Promise<Data[]> {
    try {
      return await this.dataModel
        .find({ personId: null })
        .skip(pagination?.skip)
        .limit(pagination?.limit)
        .lean();
    } catch (error) {
      catchError(error);
    }
  }
  public async findAllDataInDay(
    date: string,
    pagination?: Pagination,
  ): Promise<Data[]> {
    try {
      return await this.dataModel
        .find({ createdAt: new Date(date) })
        .skip(pagination?.skip)
        .limit(pagination?.limit)
        .lean();
    } catch (error) {
      catchError(error);
    }
  }
  public async clearAll(): Promise<any> {
    try {
      return await this.dataModel.deleteMany();
    } catch (error) {
      catchError(error);
    }
  }
}

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema, Types } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { schemaOption } from 'src/common/mongo';
import { AreaEnum } from '../interfaces';

export type DataDocument = Data & Document;

@Schema(schemaOption)
export class Data extends Base {
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    required: false,
    immutable: true,
    ref: 'Person',
  })
  personId: Types.ObjectId;

  @Prop({ type: Date, required: true, immutable: true })
  time: Date;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    required: true,
    immutable: true,
    ref: 'Image',
  })
  imageId: Types.ObjectId;

  @Prop({ type: Boolean, required: true, default: false })
  mask: boolean;

  @Prop({
    type: String,
    enum: Object.values(AreaEnum),
    required: true,
    immutable: true,
  })
  area: AreaEnum;
}

export const DataSchema = SchemaFactory.createForClass(Data);

import {
  BadRequestException,
  Body,
  Get,
  Query,
  Post,
  UploadedFile,
} from '@nestjs/common';
import {
  ApiInit,
  ControllerInit,
  DeviceAuth,
  UploadSingleAndBody,
} from 'src/common/decorators';
import { catchError } from 'src/common/exceptions';
import { Id } from 'src/modules/auth/decorators';
import { ImageService } from 'src/modules/image/services';
import { PersonService } from 'src/modules/person/services';
import { DataCreateDTO } from '../dtos';
import { Data } from '../models';
import { DataService } from '../services';

@ControllerInit('data')
@DeviceAuth()
export class DataController {
  constructor(
    private readonly dataService: DataService,
    private readonly imageService: ImageService,
    private readonly personService: PersonService,
  ) {}

  @Post()
  @ApiInit('Send data ', String)
  @UploadSingleAndBody('file', DataCreateDTO)
  public async sendData(
    @UploadedFile() file: Express.Multer.File,
    @Body() body: DataCreateDTO,
    @Id() id: string,
  ): Promise<Data> {
    try {
      const person = await this.personService.findById(body.personId);
      if (body.personId !== 'Unknown' && !person)
        throw new BadRequestException('PersonId wrong!');
      const image = await this.imageService.newImage({
        name: file.filename,
        deviceId: id,
        personId: body.personId,
        path: file.path,
      });
      const data = await this.dataService.newData({
        ...body,
        imageId: image.id,
      });
      return data;
    } catch (error) {
      catchError(error);
    }
  }

  @Get(':id')
  @ApiInit('Find data by id', Data)
  public async findOne(@Query('id') id: string): Promise<Data> {
    try {
      return await this.dataService.findById(id);
    } catch (error) {
      catchError(error);
    }
  }

  @Get()
  @ApiInit('Find all', [Data])
  public async findAll(): Promise<Data[]> {
    try {
      return await this.dataService.findAll();
    } catch (error) {
      catchError(error);
    }
  }
  @Get('date')
  @ApiInit('Find all data in day', [Data])
  public async findByDay(@Query('date') date: string): Promise<Data[]> {
    try {
      return await this.dataService.findAllDataInDay(date); //FIXME:
    } catch (error) {
      catchError(error);
    }
  }
}

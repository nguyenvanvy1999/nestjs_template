import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types, Schema as MongooseSchema } from 'mongoose';
import { Base } from 'src/modules/base/models';
import { schemaOption } from 'src/common/mongo';

export type CustomerDocument = Customer & Document;

@Schema(schemaOption)
export class Customer extends Base {
  @Prop({ type: String, required: true })
  nameOfBookingPerson: string;

  @Prop({ type: Number, required: true })
  numberOfPerson: number;

  @Prop({ type: [{ type: MongooseSchema.Types.ObjectId, ref: 'Person' }] })
  personIds: Types.ObjectId[];

  @Prop({ type: [MongooseSchema.Types.ObjectId], required: true, ref: 'Room' })
  roomIds: Types.ObjectId[];

  @Prop({ type: Date, required: true, default: Date.now() })
  checkIn: Date;

  @Prop({ type: Date, required: true })
  checkOut: Date;
}

export const CustomerSchema = SchemaFactory.createForClass(Customer);

import { Body, Get, Patch, Post } from '@nestjs/common';
import { ApiInit, ControllerInit } from 'src/common/decorators';
import { catchError } from 'src/common/exceptions';
import { ChangerPersonDTO, NewCustomerDTO } from '../dtos';
import { Customer } from '../models';
import { CustomerService } from '../services';

@ControllerInit('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Post()
  @ApiInit('Create new customer', Customer)
  public async newCustomer(@Body() data: NewCustomerDTO): Promise<Customer> {
    try {
      return await this.customerService.newCustomer(data);
    } catch (error) {
      catchError(error);
    }
  }

  @Patch('add-person')
  @ApiInit('Add new peron to customer', Customer)
  public async addPersons(@Body() body: ChangerPersonDTO): Promise<Customer> {
    try {
      return await this.customerService.addPersons(body);
    } catch (error) {
      catchError(error);
    }
  }

  @Patch('rmv-person')
  @ApiInit('Remove persons to customer', Customer)
  public async rmvPersons(@Body() body: ChangerPersonDTO): Promise<Customer> {
    try {
      return await this.customerService.removePersons(body);
    } catch (error) {
      catchError(error);
    }
  }

  @Get()
  @ApiInit('Get all customer', [Customer])
  public async findAll(): Promise<Customer[]> {
    try {
      return await this.customerService.findAll();
    } catch (error) {
      catchError(error);
    }
  }
}

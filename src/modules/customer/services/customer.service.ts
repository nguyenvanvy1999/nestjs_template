import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { catchError } from 'src/common/exceptions';
import { Pagination } from 'src/modules/base/dtos';
import { ChangerPersonDTO, NewCustomerDTO } from '../dtos';
import { Customer, CustomerDocument } from '../models';

@Injectable()
export class CustomerService {
  constructor(
    @InjectModel(Customer.name)
    private readonly customerModel: Model<CustomerDocument>,
  ) {}

  public async findById(id: string): Promise<Customer> {
    try {
      return await this.customerModel.findById(id);
    } catch (error) {
      catchError(error);
    }
  }

  public async findAll(pagination?: Pagination): Promise<Customer[]> {
    try {
      return await this.customerModel
        .find()
        .skip(pagination?.skip)
        .limit(pagination?.limit)
        .lean();
    } catch (error) {
      catchError(error);
    }
  }

  public async getPersonsInCustomer(id: string): Promise<string[]> {
    try {
      const { personIds } = await this.customerModel.findById(id);
      return personIds.map((person) => person.toHexString());
    } catch (error) {
      catchError(error);
    }
  }

  public async deleteCustomer(_id: string): Promise<Customer> {
    try {
      return await this.customerModel.findOneAndDelete({ _id });
    } catch (error) {
      catchError(error);
    }
  }

  public async newCustomer(customer: NewCustomerDTO): Promise<Customer> {
    try {
      const newCustomer = new this.customerModel({
        _id: Types.ObjectId(),
        ...customer,
        personIds: customer.personIds,
        roomIds: customer.roomIds,
      });
      await newCustomer.save();
      return newCustomer;
    } catch (error) {
      catchError(error);
    }
  }

  public async addPersons(data: ChangerPersonDTO): Promise<Customer> {
    try {
      return await this.customerModel.findOneAndUpdate(
        { _id: data.customerId },
        {
          $push: { $each: data.personIds },
          $inc: { numberOfPerson: data.personIds.length },
        },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }

  public async removePersons(data: ChangerPersonDTO): Promise<Customer> {
    try {
      return await this.customerModel.findOneAndUpdate(
        { _id: data.customerId },
        {
          $pullAll: { persons: data.personIds },
          $inc: { numberOfPerson: -data.personIds.length },
        },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }

  public async incCountPerson(
    customerId: string,
    count: number,
  ): Promise<Customer> {
    try {
      return await this.customerModel.findOneAndUpdate(
        { _id: customerId },
        { $inc: { numberOfPerson: count } },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }

  public async rmvCountPerson(
    customerId: string,
    count: number,
  ): Promise<Customer> {
    try {
      return await this.customerModel.findOneAndUpdate(
        { _id: customerId },
        { $inc: { numberOfPerson: -count } },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }

  public async updateCustomer(customerId: string, update): Promise<Customer> {
    try {
      return await this.customerModel.findOneAndUpdate(
        { _id: customerId },
        { $set: { ...update } },
        { new: true },
      );
    } catch (error) {
      catchError(error);
    }
  }
  public async clearAll(): Promise<any> {
    try {
      return await this.customerModel.deleteMany();
    } catch (error) {
      catchError(error);
    }
  }
}

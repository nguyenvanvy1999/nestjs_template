import { Test, TestingModule } from '@nestjs/testing';
import { CustomerController } from '../controllers';
import { CustomerService } from '../services';

describe('CustomerController', () => {
  let customerController: CustomerController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [CustomerController],
      providers: [CustomerService],
    }).compile();
    customerController = app.get<CustomerController>(CustomerController);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(customerController).toBeDefined();
    });
  });
});

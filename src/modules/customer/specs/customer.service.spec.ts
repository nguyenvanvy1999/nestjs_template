import { Test, TestingModule } from '@nestjs/testing';
import { CustomerService } from '../services';

describe('CustomerService', () => {
  let customerService: CustomerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CustomerService],
    }).compile();
    customerService = module.get<CustomerService>(CustomerService);
  });

  describe('define', () => {
    it('should be defined', () => {
      expect(customerService).toBeDefined();
    });
  });
});

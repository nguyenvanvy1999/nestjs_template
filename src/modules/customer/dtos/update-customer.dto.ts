import { PartialType } from '@nestjs/swagger';
import { NewCustomerDTO } from './new-customer.dto';

export class UpdateCustomerDTO extends PartialType(NewCustomerDTO) {}

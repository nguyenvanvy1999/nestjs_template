import { ApiProperty } from '@nestjs/swagger';
import { ArrayMongoId, MongoId } from 'src/common/decorators';

export class ChangerPersonDTO {
  @ApiProperty({
    description: 'CustomerId to changer',
    required: true,
    type: String,
  })
  @MongoId(true)
  customerId: string;

  @ApiProperty({
    description: 'PersonIds to changer',
    required: true,
    type: [String],
  })
  @ArrayMongoId(1)
  personIds: string[];
}

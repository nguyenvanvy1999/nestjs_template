import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';
import { number } from 'joi';
import { ArrayMongoId, CheckString } from 'src/common/decorators';

export class NewCustomerDTO {
  @ApiProperty({ description: 'Person Booking ', type: String, required: true })
  @CheckString()
  nameOfBookingPerson: string;

  @ApiProperty({
    description: 'Number person in customer',
    type: number,
    required: true,
    default: 1,
  })
  @IsInt()
  @IsNotEmpty()
  numberOfPerson: number;

  @ApiProperty({
    description: 'PersonIds in customer',
    type: [String],
    required: true,
    minItems: 1,
  })
  @ArrayMongoId(1)
  personIds: string[];

  @ApiProperty({
    description: 'RoomId of customer',
    type: String,
    required: true,
  })
  @ArrayMongoId(1)
  roomIds: string[];
}

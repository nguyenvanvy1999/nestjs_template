import {
  applyDecorators,
  UseFilters,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { WebSocketGateway } from '@nestjs/websockets';
import { WebsocketsExceptionFilter } from 'src/common/exceptions';

export const WebSocketGatewayInit = (port: number) =>
  applyDecorators(
    UseFilters(new WebsocketsExceptionFilter()),
    UsePipes(new ValidationPipe()),
    WebSocketGateway(port),
  );

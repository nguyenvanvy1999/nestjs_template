import { applyDecorators } from '@nestjs/common';
import { ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { DecoratorType } from '../interfaces';

export const ApiInit = (
  summary: string,
  type?: DecoratorType,
  resDescription?: string,
) =>
  applyDecorators(
    ApiOperation({ summary }),
    ApiOkResponse({ description: resDescription || 'OK', type: type }),
  );

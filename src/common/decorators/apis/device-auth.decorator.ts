import { applyDecorators, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { ErrorRes } from 'src/common/exceptions';
import { DeviceJwtAuthGuard } from 'src/modules/auth/guards';

export const DeviceAuth = () =>
  applyDecorators(
    UseGuards(DeviceJwtAuthGuard),
    ApiBearerAuth(),
    ApiUnauthorizedResponse({ description: 'Unauthorized', type: ErrorRes }),
  );

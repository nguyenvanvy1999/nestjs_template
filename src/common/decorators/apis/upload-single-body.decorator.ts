import { applyDecorators, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes } from '@nestjs/swagger';
import { fileFilter, storage } from 'src/common/multer';
import { DecoratorType } from '../interfaces';

export const UploadSingleAndBody = (fileName: string, body: DecoratorType) =>
  applyDecorators(
    ApiBody({ type: body }),
    ApiConsumes('multipart/form-data'),
    UseInterceptors(FileInterceptor(fileName, { storage, fileFilter })),
  );

import { applyDecorators, UseInterceptors } from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes } from '@nestjs/swagger';
import { fileFilter, storage } from 'src/common/multer';
import { DecoratorType } from '../interfaces';

export const UploadMultiAndBody = (
  fileNames: string,
  maxCount: number,
  body: DecoratorType,
) =>
  applyDecorators(
    ApiBody({ type: body }),
    ApiConsumes('multipart/form-data'),
    UseInterceptors(
      FilesInterceptor(fileNames, maxCount, { storage, fileFilter }),
    ),
  );

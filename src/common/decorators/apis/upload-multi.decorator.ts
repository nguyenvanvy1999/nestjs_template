import { applyDecorators, UseInterceptors } from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { ApiConsumes } from '@nestjs/swagger';
import { storage, fileFilter } from '../../multer';
import { ApiMultiFile } from '../../swagger';
export const UploadMulti = (name: string, count: number) =>
  applyDecorators(
    UseInterceptors(FilesInterceptor(name, count, { storage, fileFilter })),
    ApiConsumes('multipart/form-data'),
    ApiMultiFile(name),
  );

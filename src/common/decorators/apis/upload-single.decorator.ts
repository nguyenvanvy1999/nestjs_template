import { applyDecorators, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiConsumes } from '@nestjs/swagger';
import { storage, fileFilter } from '../../multer';
import { ApiFile } from '../../swagger';

export const UploadSingle = (name: string) =>
  applyDecorators(
    UseInterceptors(FileInterceptor(name, { storage, fileFilter })),
    ApiConsumes('multipart/form-data'),
    ApiFile(name),
  );

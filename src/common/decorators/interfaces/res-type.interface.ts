import { Type } from '@nestjs/common';

export type DecoratorType = Type<unknown> | Function | [Function] | string;

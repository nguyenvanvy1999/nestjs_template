import { applyDecorators } from '@nestjs/common';
import { IsMongoId, IsNotEmpty } from 'class-validator';

export const MongoId = (require?: boolean) => {
  const _ = [IsMongoId()];
  require && _.push(IsNotEmpty());
  return applyDecorators(..._);
};

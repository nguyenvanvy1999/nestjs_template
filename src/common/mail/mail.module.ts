import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import { mailConfig } from './mail.config';
import { MailService } from './services/mail.service';

@Module({
  imports: [MailerModule.forRootAsync(mailConfig)],
  providers: [MailService],
  exports: [MailService],
})
export class MailModule {}

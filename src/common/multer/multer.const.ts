export const maxSize: number = 1024 * 1024 * 5;

export const fileMime = [
  'image/jpg',
  'image/png',
  'image/jpeg',
  'video/mp4',
  'video/avi',
  'video/mov',
  'video/flv',
  'video/wmv',
];

export const fileExt: string[] = [
  '.jpg',
  '.png',
  '.jpeg',
  '.mp4',
  '.mov',
  '.flv',
  '.wmv',
  '.avi',
];

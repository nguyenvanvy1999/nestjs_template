import path from 'path';
import { UnsupportedMediaTypeException } from '@nestjs/common';
import fs from 'fs';
import { FileFilterCallback, diskStorage } from 'multer';
import { Request } from 'express';
import { fileExt, fileMime } from './multer.const';
import { extname } from 'path';
import { v4 } from 'uuid';

const checkFile = (file: Express.Multer.File): boolean => {
  const mime = fileMime.includes(file.mimetype);
  const ext = fileExt.includes(path.extname(file.originalname));
  return ext && mime;
};
export const fileFilter = (
  req: Request,
  file: Express.Multer.File,
  cb: FileFilterCallback,
): void => {
  if (!checkFile(file))
    return cb(new UnsupportedMediaTypeException('Only images are allowed'));
  cb(null, true);
};
export const storage = diskStorage({
  destination(
    req: Request,
    file: Express.Multer.File,
    cb: (error: Error | null, destination: string) => void,
  ): string | void {
    let dir1: string;
    const folder = req.url.split('/')[1];
    folder === 'data'
      ? (dir1 = `./uploads/data/${req.body.personId}`)
      : (dir1 = `./uploads/person`);
    fs.mkdirSync(dir1, { recursive: true });

    const device: any = req.user;
    const dir = `./uploads/${device._id}`;
    fs.mkdirSync(dir, { recursive: true });
    return cb(null, dir);
  },
  filename(
    req: Request,
    file: Express.Multer.File,
    cb: (error: Error | null, filename: string) => void,
  ): void {
    const ext = extname(file.originalname);
    cb(null, `${v4()}_${ext}`);
  },
});

export const environments = [
  'development',
  'concurrent',
  'unit',
  'integration',
  'test',
  'qa',
  'staging',
  'production',
];

import { Module } from '@nestjs/common';
import { AppConfigService } from './env.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { configModuleSetup } from './env.config';
@Module({
  imports: [ConfigModule.forRoot(configModuleSetup)],
  providers: [AppConfigService, ConfigService],
  exports: [AppConfigService, ConfigService],
})
export class AppConfigModule {}

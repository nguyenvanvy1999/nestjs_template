import { envValidate } from './env.validate';

export const configModuleSetup = {
  cache: true,
  isGlobal: true,
  envFilePath: '.env',
  validationSchema: envValidate,
};

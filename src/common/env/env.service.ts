import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
@Injectable()
export class AppConfigService extends ConfigService {
  constructor() {
    super();
  }
  get isDebug(): boolean {
    return this.get<boolean>('DEBUG');
  }
  get isSendMail(): boolean {
    return this.get<boolean>('SEND_MAIL');
  }
  get salt(): number {
    return this.get<number>('SALT');
  }
}

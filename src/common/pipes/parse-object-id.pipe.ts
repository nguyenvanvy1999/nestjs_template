import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { Types } from 'mongoose';

@Injectable()
export class ParseObjectIdPipe
  implements PipeTransform<string, Types.ObjectId>
{
  transform(value: string, metadata: ArgumentMetadata) {
    try {
      if (!Types.ObjectId.isValid(value)) {
        throw new BadRequestException(
          `$value is not a valid mongoose object id`,
        );
      }
      return Types.ObjectId(value);
    } catch (error) {
      throw error;
    }
  }
}

export interface MongoOption {
  [option: string]: boolean;
}

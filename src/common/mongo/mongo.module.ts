import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppConfigModule, AppConfigService } from '../env';
import { mongoOption } from './mongo.const';
@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [AppConfigModule],
      useFactory: (config: AppConfigService) => ({
        uri: config.get<string>('MONGO_URI'),
        ...mongoOption,
      }),
      inject: [AppConfigService],
    }),
  ],
})
export class MongoModule {}

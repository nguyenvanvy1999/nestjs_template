import { MongoOption } from './option.interface';

export const mongoOption: MongoOption = {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  ignoreUndefined: true,
  useFindAndModify: false,
};

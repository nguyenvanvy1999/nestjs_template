import { Global, Module } from '@nestjs/common';
import { AppConfigModule } from './env';
import { MailModule } from './mail';
import { MongoModule } from './mongo';

@Global()
@Module({
  imports: [AppConfigModule, MailModule, MongoModule],
  providers: [],
  exports: [AppConfigModule, MailModule, MongoModule],
})
export class CommonModule {}

<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>


## Description

[Mesh](https://mesh.vn/) Resort camera manager built with [Nestjs](https://nestjs.com/) and [MongoDB](https://www.mongodb.com/)

## Installation

```bash
npm install
```

## Running the app

```bash
# development
npm run start

# watch mode
npm run start:dev

# production mode
npm run start:prod
```

## Test

```bash
# unit tests
npm run test

# e2e tests
npm run test:e2e

# test coverage
npm run test:cov
```

## Technology stack

- Typescript

- Nest.js

- RESTful API

- MongoDB

- Python FlashAPI Server to training face recognition model

## License

[MIT licensed](LICENSE).
